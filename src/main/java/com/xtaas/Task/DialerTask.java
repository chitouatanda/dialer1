package com.xtaas.Task;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.xtaas.config.Constants;
import com.xtaas.domain.Prospect;
import com.xtaas.service.AgentQueueService;
import com.xtaas.service.CallService;
import com.xtaas.service.CampaignService;
import com.xtaas.service.ProspectService;
import com.xtaas.service.dto.CampaignDTO;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.dto.QueuedProspectDTO;
import com.xtaas.service.util.LogUtil;
import com.xtaas.service.util.XtaasConstants;
import com.xtaas.valueobject.DialerMode;
import com.xtaas.valueobject.ProspectCallStatus;

@Component
public class DialerTask {
	private final Logger log = LoggerFactory.getLogger(DialerTask.class);

	private final AgentQueueService agentQueueService;
	private final CampaignService campaignService;
	private final ProspectService prospectService;
	private final CallService callService;
	private final Map<String, CampaignDTO> campaignMap = new HashMap<>();
	private final Map<String, List<String>> campaignCallsMap = new HashMap<>();
	private List<String> activeCampaigns = new ArrayList<>();

	public DialerTask(AgentQueueService agentQueueService,
			CampaignService campaignService, ProspectService prospectService, CallService callService) {
		this.agentQueueService = agentQueueService;
		this.campaignService = campaignService;
		this.prospectService = prospectService;
		this.callService = callService;
	}

	public void processCampaigns() {
		List<String> inactiveCampaigns = activeCampaigns;
		activeCampaigns = agentQueueService.getActiveCampaigns();
		log.info("Fetching active campaigns = {} ", activeCampaigns);
		inactiveCampaigns.removeAll(activeCampaigns);
		if (inactiveCampaigns.size() > 0) {
			processInactiveCampains(inactiveCampaigns);
		}
		if (activeCampaigns.size() > 0) {
			processActiveCampains(activeCampaigns);
		}

	}

	private void processInactiveCampains(List<String> inactiveCampaigns) {
		log.info("Processing inactive campaigns: {}", inactiveCampaigns.size());
		for (String inactiveCampaign : inactiveCampaigns) {
			log.info("Cleaning campaign details for inactive campaign id: {}", inactiveCampaign);
			List<String> callSidList = campaignCallsMap.get(inactiveCampaign);
			CampaignDTO campaignDTO = campaignMap.get(inactiveCampaign);
			String callProvider = null;
			if(campaignDTO.getAutoModeCallProvider()!=null && !campaignDTO.getAutoModeCallProvider().isEmpty()) {
				callProvider = campaignDTO.getAutoModeCallProvider();
			}else {
				callProvider = campaignDTO.getCallControlProvider();
			}
			if (callProvider.equalsIgnoreCase("Plivo")) {
				if (!campaignDTO.isCallAbandon()) {
					callService.cancelPlivoPendingCalls(callSidList);
				}
			} else if (callProvider.equalsIgnoreCase("Telnyx")) {
				// commented for verification of AUTO Hangup prospects
				// callService.cancelTelnyxPendingCalls(callSidList);
			} else if (campaignDTO.getCallControlProvider().equalsIgnoreCase("Signalwire")) {
				// callService.emptySignalwireCallStatusMap();
				callService.cancelSignalwirePendingCalls(callSidList);
			} else {
				callService.cancelPendingCalls(callSidList);
			}
			campaignCallsMap.remove(inactiveCampaign);
			campaignMap.remove(inactiveCampaign);
		}
	}

	private void processActiveCampains(List<String> activeCampaigns) {
		log.info("Processing active campaigns: {}", activeCampaigns.size());
		for (String campaignId : activeCampaigns) {
			log.info("Processing active campaign: [{}]", campaignId);
			CampaignDTO campaignDTO = campaignMap.get(campaignId);
			if (campaignDTO == null) {
				log.info("Loading campaign details for campaign id: {}", campaignId);
				campaignDTO = campaignService.getCampaign(campaignId);
				campaignMap.put(campaignId, campaignDTO);
				log.info("Refreshed campaign details: ", campaignDTO.toString());
				campaignCallsMap.put(campaignId, new ArrayList<>());
			}
			try {
				if (!campaignDTO.getDialerMode().equals(DialerMode.RESEARCH)) {
					processProspects(campaignDTO);
				}
			} catch (Exception e) {
				log.error("Failed processing prospects for campaign: {} with message: {}", e.getMessage());
				log.info(LogUtil.extractStackTrace(e));
			}
			log.info("Finished Processing active campaign: [{}]", campaignId);
		}
	}

	@Async
	public void processProspects(CampaignDTO campaignDTO) throws UnsupportedEncodingException, URISyntaxException {
		synchronized (campaignDTO) {
			List<String> campaignCalls = new ArrayList<>();
			String callProvider = null;
			if(campaignDTO.getAutoModeCallProvider()!=null && !campaignDTO.getAutoModeCallProvider().isEmpty()) {
				callProvider = campaignDTO.getAutoModeCallProvider();
			}else {
				callProvider = campaignDTO.getCallControlProvider();
			}
			if (callProvider.equalsIgnoreCase("PLivo")) {
				campaignCalls = callService.getPlivoPendingCalls(campaignCallsMap.get(campaignDTO.getId()));
			} else if (callProvider.equalsIgnoreCase("Telnyx")) {
				campaignCalls = callService.getTelnyxPendingCalls(campaignCallsMap.get(campaignDTO.getId()));
			} else if (campaignDTO.getCallControlProvider().equalsIgnoreCase("Signalwire")) {
				campaignCalls = callService.getSignalwirePendingCalls(campaignCallsMap.get(campaignDTO.getId()));
				log.info("Pending signalwire calls campaignId: {} size : {}, callsIds : {}", campaignDTO.getId(), campaignCalls.size(), campaignCallsMap.get(campaignDTO.getId()));
			} else {
				campaignCalls = callService.getPendingCalls(campaignCallsMap.get(campaignDTO.getId()));
			}
			
			campaignCallsMap.put(campaignDTO.getId(), campaignCalls);
			log.info("Current pending jobs for campaign: {} are: {}", campaignDTO.getId(), campaignCalls.size());
			QueuedProspectDTO queuedProspectDTO = null;
			if (campaignDTO.getDialerMode().equals(DialerMode.RESEARCH_AND_DIAL)) {
				queuedProspectDTO = prospectService.getCallableProspects(campaignDTO.getId(), campaignCalls.size(),
						"DIRECT");
				if (queuedProspectDTO != null && queuedProspectDTO.getQueuedProspects().size() == 0) {
					queuedProspectDTO = prospectService.getCallableProspects(campaignDTO.getId(), campaignCalls.size(),
							"INDIRECT");
				}
			} else {
				queuedProspectDTO = prospectService.getCallableProspects(campaignDTO.getId(), campaignCalls.size(), "");
			}
			List<ProspectCallDTO> prospectCallList = queuedProspectDTO.getQueuedProspects();
			log.info("New prospect count for campaign: {} are: {} and delta size : {} ", campaignDTO.getId(), prospectCallList.size(), queuedProspectDTO.getDelta());

			int prospectDeltaCount = queuedProspectDTO.getDelta();
			log.info("Call Abandoned : {} ", campaignDTO.isCallAbandon());
			if (campaignDTO.isCallAbandon()) {
				log.info(
						"There are total : {} of pending calls, out of which : {} could have been canceled instead of abandon as the "
								+ "dontCancelOnNegativeDelta is true on campaign : {} ",
						queuedProspectDTO.getQueuedProspects(), prospectDeltaCount, campaignDTO.getId());

			}
			if (prospectDeltaCount < 0 && campaignDTO.isCallAbandon()) {
				if (callProvider.equalsIgnoreCase("Plivo")) {
					List<String> callSidList = campaignCallsMap.get(campaignDTO.getId());
					// callService.cancelPendingCalls(callSidList, -prospectDeltaCount);
				} else if (callProvider.equalsIgnoreCase("Telnyx")) {
					List<String> callSidList = campaignCallsMap.get(campaignDTO.getId());
					// callService.cancelTelnyxPendingCalls(callSidList, -prospectDeltaCount);
				} else if (campaignDTO.getCallControlProvider().equalsIgnoreCase("Signalwire")) {
					List<String> callSidList = campaignCallsMap.get(campaignDTO.getId());
					// callService.cancelTelnyxPendingCalls(callSidList, -prospectDeltaCount);
				} else {
					List<String> callSidList = campaignCallsMap.get(campaignDTO.getId());
					// callService.cancelPendingCalls(callSidList, -prospectDeltaCount);
				}
			} else if (prospectDeltaCount >= 0) {
				for (ProspectCallDTO prospectCallDTO : prospectCallList) {
					processProspect(campaignCalls, prospectCallDTO, campaignDTO);
				}
			}
		}
	}

	@Async
	public void processProspect(List<String> campaignCalls, ProspectCallDTO prospectCallDTO, CampaignDTO campaignDTO) {
		try {
			ProspectCallStatus prospectCallStatus = callService.processProspectCall(prospectCallDTO, campaignDTO);
			Prospect prospect = prospectCallDTO.getProspect();
			if (prospectCallStatus == ProspectCallStatus.CALL_DIALED) {
				if (campaignDTO.getDialerMode().equals(DialerMode.POWER)
						|| campaignDTO.getDialerMode().equals(DialerMode.HYBRID)) {
					campaignCalls.add(prospectCallDTO.getTwilioCallSid());
					// campaignCallsMap.put(campaignDTO.getId(), campaignCalls);
					updateAndRegisterProspect(prospectCallStatus, prospectCallDTO);
				} else if (campaignDTO.getDialerMode().equals(DialerMode.RESEARCH_AND_DIAL)) {
					if (prospect.getProspectType().equalsIgnoreCase(Constants.PROSPECT_TYPE.POWER.name())) {
						campaignCalls.add(prospectCallDTO.getTwilioCallSid());
						// campaignCallsMap.put(campaignDTO.getId(), campaignCalls);
						updateAndRegisterProspect(prospectCallStatus, prospectCallDTO);
					} else if (prospect.getProspectType().equalsIgnoreCase(Constants.PROSPECT_TYPE.PREVIEW.name())) {
						updateAndRegisterProspect(prospectCallStatus, prospectCallDTO);
						prospectService.allocateAgentPreviewMode(prospectCallDTO.getTwilioCallSid(), campaignDTO.isCallAbandon());
					} else {
						callService.registerCallDetails(prospectCallDTO.getTwilioCallSid(), prospectCallDTO);
						prospectService.allocateAgentPreviewMode(prospectCallDTO.getTwilioCallSid(), campaignDTO.isCallAbandon());
					}
				} else {
					updateAndRegisterProspect(prospectCallStatus, prospectCallDTO);
					prospectService.allocateAgentPreviewMode(prospectCallDTO.getTwilioCallSid(), campaignDTO.isCallAbandon());
				}
			} else {
				prospectService.updateProspect(prospectCallDTO.getProspectCallId(), null, null, prospectCallStatus,
						prospectCallDTO.getCallRetryCount(), prospectCallDTO.getDailyCallRetryCount(), null);
			}
		} catch (Exception e) {
			log.error("Error occured while making call {} for campaign {}", prospectCallDTO.getProspectCallId(),
					campaignDTO.getName());
			log.error("Exception is : {} ", e);
			prospectService.updateProspect(prospectCallDTO.getProspectCallId(), null, null,
					ProspectCallStatus.UNKNOWN_ERROR, prospectCallDTO.getCallRetryCount(),
					prospectCallDTO.getDailyCallRetryCount(), null);
		}
	}

	private void updateAndRegisterProspect(ProspectCallStatus prospectCallStatus, ProspectCallDTO prospectCallDTO) {
		prospectService.updateProspect(prospectCallDTO.getProspectCallId(),
				prospectCallDTO.getProspectInteractionSessionId(), prospectCallDTO.getTwilioCallSid(),
				prospectCallStatus, prospectCallDTO.getCallRetryCount(), prospectCallDTO.getDailyCallRetryCount(),
				prospectCallDTO.getOutboundNumber());
		callService.registerCallDetails(prospectCallDTO.getTwilioCallSid(), prospectCallDTO);
	}

	public boolean refreshCampaignCacheByCampaignId(List<String> campaignIds) {
		if (campaignIds != null && campaignIds.size() > 0) {
			for (String campaignId : campaignIds) {
				log.info("##### Campaign cache refresh started for [{}] #####", campaignId);
				if (campaignMap.containsKey(campaignId)) {
					campaignMap.remove(campaignId);
				}
				log.info("##### Campaign cache refresh finished for [{}] #####", campaignId);
			}
		}
		return true;
	}

	public boolean refreshAllCampaignCache() {
		log.info("##### All campaign cache refresh started. #####");
		campaignMap.clear();
		log.info("##### All campaign cache refresh finished. #####");
		return true;
	}
}
