package com.xtaas.Task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.xtaas.service.TwilioRecordingService;

@Component
public class TwilioRecordingsDeleterTask {

	private final Logger log = LoggerFactory.getLogger(TwilioRecordingsDeleterTask.class);
	private final TwilioRecordingService twilioRecordingService;

	public TwilioRecordingsDeleterTask(TwilioRecordingService twilioRecordingService) {
		this.twilioRecordingService = twilioRecordingService;
	}

	public void deleteRecordings() {
		log.info("Starting the twilio recordings deleter task.");
		twilioRecordingService.deleteRecordings();
	}

}
