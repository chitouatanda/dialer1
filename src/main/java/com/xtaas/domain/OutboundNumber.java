package com.xtaas.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "outboundnumber")
public class OutboundNumber {
	@Id
	private String id;
	private String phoneNumber;
	private String displayPhoneNumber;
	private int areaCode;
	private int isdCode;
	private String provider;
	private String partnerId;
	private String bucketName;
	private String voiceMessage;
	private String timeZone;
	private boolean isRedialNumber;
	private String countryCode;
	private String countryName;
	private int pool;

	protected OutboundNumber() {
	}

	public OutboundNumber(String phoneNumber, String displayPhoneNumber, int areaCode, String timeZone, int isdCode,
			String provider, String partnerId, String bucketName, String voiceMessage, String countryCode,
			String countryName, int pool) {
		setPhoneNumber(phoneNumber);
		setDisplayPhoneNumber(displayPhoneNumber);
		setAreaCode(areaCode);
		setIsdCode(isdCode);
		setProvider(provider);
		setPartnerId(partnerId);
		setBucketName(bucketName);
		setVoiceMessage(voiceMessage);
		setRedialNumber(false);
		setTimeZone(timeZone);
		setCountryCode(countryCode);
		setCountryName(countryName);
		setPool(pool);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDisplayPhoneNumber() {
		return displayPhoneNumber;
	}

	public void setDisplayPhoneNumber(String displayPhoneNumber) {
		this.displayPhoneNumber = displayPhoneNumber;
	}

	public int getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(int areaCode) {
		this.areaCode = areaCode;
	}

	public int getIsdCode() {
		return isdCode;
	}

	public void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getVoiceMessage() {
		return voiceMessage;
	}

	public void setVoiceMessage(String voiceMessage) {
		this.voiceMessage = voiceMessage;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public boolean isRedialNumber() {
		return isRedialNumber;
	}

	public void setRedialNumber(boolean isRedialNumber) {
		this.isRedialNumber = isRedialNumber;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getPool() {
		return pool;
	}

	public void setPool(int pool) {
		this.pool = pool;
	}

	@Override
	public String toString() {
		return "OutboundNumber [id=" + id + ", phoneNumber=" + phoneNumber + ", displayPhoneNumber="
				+ displayPhoneNumber + ", areaCode=" + areaCode + ", isdCode=" + isdCode + ", provider=" + provider
				+ ", partnerId=" + partnerId + ", bucketName=" + bucketName + ", voiceMessage=" + voiceMessage
				+ ", timeZone=" + timeZone + ", isRedialNumber=" + isRedialNumber + ", countryCode=" + countryCode
				+ ", countryName=" + countryName + ", pool=" + pool + "]";
	}

}
