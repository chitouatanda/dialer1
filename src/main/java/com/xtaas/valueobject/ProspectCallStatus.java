package com.xtaas.valueobject;

public enum ProspectCallStatus {
		QUEUED, //Ready for calling
		LOCAL_DNC_ERROR, //Phone number could not pass local DNC check
		NATIONAL_DNC_ERROR, //Phone number could not pass National DNC check
		UNKNOWN_ERROR, //Runtime Error Occurred
		MAX_RETRY_LIMIT_REACHED, //Retried for max limit
		CALL_TIME_RESTRICTION, //TZ and Holiday restrictions did not pass. Callback again later
		CALL_NO_ANSWER, //Call not answered
		CALL_BUSY, //call busy
		CALL_CANCELED, //Call canceled
		CALL_FAILED, //Call Failed
		CALL_MACHINE_ANSWERED, //Call Answered by Machine
		CALL_ABANDONED, //Call has been Abandoned due to Agent's unavailability
		CALL_PREVIEW, //Only applicable for Preview Mode where Dialer has pushed the prospect to agent but call not yet initiated
		CALL_DIALED, //Call has been dialed but not yet picked up
		// CALL_INPROGRESS, //Call in progress
		CALL_COMPLETE, //Conversation finished
		WRAPUP_COMPLETE, //Agent submitted the prospect details after filling in call information
		QA_INITIATED, //Prospect Call's QA initiated
		QA_COMPLETE, //Prospect Call QAed 
		SECONDARY_QA_INITIATED,	// DATE : 05/06/2017	REASON : Prospect Call's Secondary QA initiated
		QA_WRAPUP_COMPLETE,
		CLIENT_DELIVERED,
		CLIENT_REJECTED,
		QA_REJECTED,
		PRIMARY_REVIEW,
		SECONDARY_REVIEW,
		INDIRECT_CALLING
	}