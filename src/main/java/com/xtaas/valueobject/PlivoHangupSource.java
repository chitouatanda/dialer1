package com.xtaas.valueobject;

/**
 * PLIVO Call Hangup Source
 */
public enum PlivoHangupSource {

	// Caller, Callee, Plivo, Carrier, API Request, Answer XML, Error, Unknown

	CALLER, CALLEE, PLIVO, CARRIER, API_REQUEST, ANSWER_XML, ERROR, UNKNOWN;

	@Override
	public String toString() {
		switch (this) {
		case CALLER:
			return "Caller";
		case CALLEE:
			return "Callee";
		case PLIVO:
			return "Plivo";
		case CARRIER:
			return "Carrier";
		case API_REQUEST:
			return "API Request";
		case ANSWER_XML:
			return "Answer XML";
		case ERROR:
			return "Error";
		case UNKNOWN:
			return "Unknown";
		default:
			break;
		}
		return null;
	}

}
