package com.xtaas.valueobject;

public class PlivoStatusConstants {

    // PLIVO Call Statuses
    public static final String PLIVO_CALL_STATUS_QUEUED = "queued";
    public static final String PLIVO_CALL_STATUS_RINGING = "ringing";
    public static final String PLIVO_CALL_STATUS_INPROGRESS = "in-progress";
    public static final String PLIVO_CALL_STATUS_COMPLETED = "completed";
    public static final String PLIVO_CALL_STATUS_BUSY = "busy";
    public static final String PLIVO_CALL_STATUS_FAILED = "failed";
    public static final String PLIVO_CALL_STATUS_NO_ANSWER = "no-answer";
    public static final String PLIVO_CALL_STATUS_CANCELED = "canceled";
    public static final String PLIVO_CALL_MACHINE_ANSWERED = "machine";

}
