package com.xtaas.valueobject;

/**
 * PLIVO Call Hangup Causes
 */
public enum PlivoHangupCause {

	// No Answer, Busy Line, Cancelled, Rejected, Normal Hangup, Ring Timeout
	// Reached, Machine Detected, Unknown

	NO_ANSWER, BUSY_LINE, CANCELLED, REJECTED, NORMAL_HANGUP, RING_TIMEOUT_REACHED, MACHINE_DETECTED, UNKNOWN;

	@Override
	public String toString() {
		switch (this) {
		case NO_ANSWER:
			return "No Answer";
		case BUSY_LINE:
			return "Busy Line";
		case CANCELLED:
			return "Cancelled";
		case REJECTED:
			return "Rejected";
		case NORMAL_HANGUP:
			return "Normal Hangup";
		case RING_TIMEOUT_REACHED:
			return "Ring Timeout Reached";
		case MACHINE_DETECTED:
			return "Machine Detected";
		case UNKNOWN:
			return "Unknown";
		default:
			break;
		}
		return null;
	}

}
