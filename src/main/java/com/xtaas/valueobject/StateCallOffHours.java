package com.xtaas.valueobject;

public class StateCallOffHours {
	private Integer startOffCallHour;
	private Integer endOffCallHour;

	public StateCallOffHours(Integer startOffCallHour, Integer endOffCallHour) {
		this.startOffCallHour = startOffCallHour;
		this.endOffCallHour = endOffCallHour;
	}

	/**
	 * @return the startOffCallHour
	 */
	public Integer getStartOffCallHour() {
		return startOffCallHour;
	}

	/**
	 * @return the endOffCallHour
	 */
	public Integer getEndOffCallHour() {
		return endOffCallHour;
	}

	@Override
	public String toString() {
		return "StateCallOffHours [startOffCallHour=" + startOffCallHour + ", endOffCallHour=" + endOffCallHour + "]";
	}

}