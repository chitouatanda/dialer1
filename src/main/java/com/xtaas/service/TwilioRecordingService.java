package com.xtaas.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.common.collect.Range;
import com.twilio.base.ResourceSet;
import com.twilio.http.HttpMethod;
import com.twilio.rest.api.v2010.account.IncomingPhoneNumber;
import com.twilio.rest.api.v2010.account.Recording;
import com.twilio.rest.api.v2010.account.RecordingReader;
import com.xtaas.service.dto.RecordingFilterDTO;
import com.xtaas.service.util.LogUtil;

@Service
public class TwilioRecordingService {

	private final Logger log = LoggerFactory.getLogger(TwilioRecordingService.class);

	@Async
	public void deleteRecordings() {
		log.info("deleteRecordings - Starting the twilio recordings deleter.");
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, -10);
			ResourceSet<Recording> recordings = Recording.reader()
					.setDateCreated(Range.lessThan(new DateTime(calendar.getTime()))).read();
			for (Recording record : recordings) {
				try {
					Recording.deleter(record.getSid()).delete();
				} catch (Exception e) {
					log.error("deleteRecordings - Error occurred while deleting [{}] recording. Reason - [{}]",
							record.getSid(), e.getMessage());
					log.info(LogUtil.extractStackTrace(e));
				}
			}
		} catch (Exception e) {
			log.error("deleteRecordings - Error occurred while fetching twilio recordings. Reason - [{}]",
					e.getMessage());
			log.info(LogUtil.extractStackTrace(e));
		}
		log.info("deleteRecordings - Finished the twilio recordings deleter.");
	}

	@Async
	public void deleteRecordings(RecordingFilterDTO recordingFilterDTO) {
		log.info("deleteRecordings - Starting the twilio recordings deleter.");
		try {
			Date startDate = null;
			Date endDate = null;
			Date dateCreated = null;
			boolean isFilterApplied = false;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			RecordingReader recordingReader = Recording.reader();
			if (recordingFilterDTO.getCallSid() != null && !recordingFilterDTO.getCallSid().isEmpty()) {
				recordingReader.setCallSid(recordingFilterDTO.getCallSid());
				isFilterApplied = true;
			} else if (recordingFilterDTO.getDateCreated() != null && !recordingFilterDTO.getDateCreated().isEmpty()) {
				dateCreated = dateFormat.parse(recordingFilterDTO.getDateCreated());
				recordingReader.setDateCreated(new DateTime(dateCreated));
				isFilterApplied = true;
			} else if (recordingFilterDTO.getStartDate() != null && !recordingFilterDTO.getStartDate().isEmpty()
					&& recordingFilterDTO.getEndDate() != null && !recordingFilterDTO.getEndDate().isEmpty()) {
				startDate = dateFormat.parse(recordingFilterDTO.getStartDate());
				endDate = dateFormat.parse(recordingFilterDTO.getEndDate());
				recordingReader.setDateCreated(Range.open(new DateTime(startDate), new DateTime(endDate)));
				isFilterApplied = true;
			}
			if (isFilterApplied) {
				ResourceSet<Recording> recordings = recordingReader.read();
				for (Recording record : recordings) {
					try {
						Recording.deleter(record.getSid()).delete();
					} catch (Exception e) {
						log.error("deleteRecordings - Error occurred while deleting [{}] recording. Reason - [{}]",
								record.getSid(), e.getMessage());
						log.info(LogUtil.extractStackTrace(e));
					}
				}
			} else {
				log.info("deleteRecordings - Filter not applied to delete twilio recordings.");
			}
		} catch (Exception e) {
			log.error("deleteRecordings - Error occurred while fetching twilio recordings. Reason - [{}]",
					e.getMessage());
			log.info(LogUtil.extractStackTrace(e));
		}
		log.info("deleteRecordings - Finished the twilio recordings deleter.");
	}

	public void resetMessageWebhook() {
		ResourceSet<IncomingPhoneNumber> incomingPhoneNumbers = IncomingPhoneNumber.reader().read();
		for (IncomingPhoneNumber phoneNumber : incomingPhoneNumbers) {
			if (phoneNumber.getSmsUrl() != null && !phoneNumber.getSmsUrl().toString().isEmpty()) {
				// set phone number messaging webhook as empty string
				IncomingPhoneNumber.updater(phoneNumber.getSid()).setSmsMethod(HttpMethod.POST).setSmsUrl("").update();
				log.info("Message URL updated for [{}] Phone Number.", phoneNumber.getPhoneNumber());
			}
		}
	}
}
