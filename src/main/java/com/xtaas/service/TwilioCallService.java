package com.xtaas.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.twilio.http.HttpMethod;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.CallCreator;
import com.twilio.type.PhoneNumber;
import com.xtaas.service.dto.CampaignDTO;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.util.XtaasConstants;

@Service
public class TwilioCallService {
	private final Logger log = LoggerFactory.getLogger(TwilioCallService.class);

	private final String callStatusCallback;
	private final int ringTimeout;
	private final String thinQDomainUrl;
	private final String thinQId;
	private final String thinQToken;
	private final String telnyxDomainUrl;
	private final String telnyxToken;

	public TwilioCallService() {
		callStatusCallback = System.getenv(XtaasConstants.TWILIO_CALLSTATUS_CALLBACK);
		ringTimeout = parseInteger(System.getenv(XtaasConstants.TWILIO_RING_TIMEOUT));
		thinQDomainUrl = System.getenv(XtaasConstants.THINQ_DOMAIN_URL);
		thinQId = System.getenv(XtaasConstants.THINQ_ID);
		thinQToken = System.getenv(XtaasConstants.THINQ_TOKEN);
		this.telnyxDomainUrl = System.getenv(XtaasConstants.TELNYX_DOMAIN_URL);
		this.telnyxToken = System.getenv(XtaasConstants.TELNYX_TOKEN);
	}

	private int parseInteger(String value) {
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			return 0;
		}
	}

	public String placeCall(ProspectCallDTO prospectCallDTO, String fromNumber, String url, CampaignDTO campaignDTO)
			throws URISyntaxException {
		String toNumber = prospectCallDTO.getProspect().getPhone();
		String dataSlice = prospectCallDTO.getDataSlice();
		int callRetryCount = prospectCallDTO.getCallRetryCount();
		// boolean directPhone = prospectCallDTO.getProspect().isDirectPhone();
		boolean useSlice = campaignDTO.isUseSlice();
		boolean disableAMDOnQueued = campaignDTO.isDisableAMDOnQueued();
		boolean enableMachineAnsweredDetection = campaignDTO.isEnableMachineAnsweredDetection();
		if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("ThinQ")) {
			toNumber = createThinQNumber(toNumber, prospectCallDTO.getProspect().getCountry());
			log.debug("Created ThinQ sip number: {}", toNumber);
		} else if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("Telnyx")) {
			toNumber = createTelnyxNumber(toNumber, prospectCallDTO.getProspect().getCountry());
			log.debug("Created Telnyx sip number: {}", toNumber);
		}
		String callbackURL = callStatusCallback + "?MachineAnsweredDetection="
				+ campaignDTO.isEnableMachineAnsweredDetection();
		CallCreator callCreator = null;
		if (enableMachineAnsweredDetection && useSlice && disableAMDOnQueued && callRetryCount > 0 && dataSlice != null
				&& dataSlice.equalsIgnoreCase("slice1")) {
			/*
			 * AMD on slice1 data except Queued -- enableMachineAnsweredDetection is true,
			 * useSlice is true, disableAMDOnQueued is true, callRetryCount > 0 (i.e. 2nd
			 * retry) & dataSlice is slice1
			 */
			callCreator = twilioInitiateAMDCall(callCreator, fromNumber, toNumber, url, callbackURL);
		} else if (enableMachineAnsweredDetection && useSlice && !disableAMDOnQueued && dataSlice != null
				&& dataSlice.equalsIgnoreCase("slice1")) {
			/*
			 * AMD on all slice1 data -- enableMachineAnsweredDetection is true, useSlice is
			 * true, disableAMDOnQueued is false & dataSlice is slice1
			 */
			callCreator = twilioInitiateAMDCall(callCreator, fromNumber, toNumber, url, callbackURL);
		} else if (enableMachineAnsweredDetection && !useSlice) {
			/*
			 * AMD on all calls -- enableMachineAnsweredDetection is true & useSlice is
			 * false
			 */
			callCreator = twilioInitiateAMDCall(callCreator, fromNumber, toNumber, url, callbackURL);
		} else {
			// NO AMD
			callCreator = Call.creator(new PhoneNumber(toNumber), new PhoneNumber(fromNumber), new URI(url))
					.setStatusCallback(callbackURL).setStatusCallbackMethod(HttpMethod.POST)
					.setStatusCallbackEvent(Arrays.asList("initiated", "ringing", "answered", "completed"));
		}

		if (ringTimeout != 0) {
			callCreator = callCreator.setTimeout(ringTimeout);
		}
		return callCreator.create().getSid();
	}

	private CallCreator twilioInitiateAMDCall(CallCreator callCreator, String fromNumber, String toNumber,
			String answerUrl, String callbackURL) throws URISyntaxException {
		callCreator = Call.creator(new PhoneNumber(toNumber), new PhoneNumber(fromNumber), new URI(answerUrl))
				.setMachineDetection("Enable").setStatusCallback(callbackURL).setStatusCallbackMethod(HttpMethod.POST)
				.setStatusCallbackEvent(Arrays.asList("initiated", "ringing", "answered", "completed"));
		return callCreator;
	}

	public String getCallStatus(String callSid) {
		return Call.fetcher(callSid).fetch().getStatus().toString();
	}

	public void cancelCall(String callSid) {
		Call.updater(callSid).setStatus(Call.UpdateStatus.COMPLETED).update();
	}

	private String createThinQNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber;
		}
		StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@").append(thinQDomainUrl)
				.append("?X-account-id=").append(thinQId).append("&X-account-token=").append(thinQToken);
		return thinQNumber.toString();
	}

	private String createTelnyxNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber;
		}
		StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@").append(telnyxDomainUrl)
				.append("?X-Telnyx-Token=").append(telnyxToken);
		return thinQNumber.toString();
	}

	private static String getSimpleFormattedUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d.]", ""); // removes all non-numeric chars
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = "+1" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}
}
