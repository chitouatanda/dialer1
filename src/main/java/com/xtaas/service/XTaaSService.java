package com.xtaas.service;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.xtaas.service.util.XtaasConstants;
import com.xtaas.service.util.AppManager.App;

@Service
public class XTaaSService extends HttpService {
	private final String baseUrl;
	// private final String authorization;
	private final AuthTokenGeneratorService authTokenGeneratorService;

	public XTaaSService(RestTemplate restTemplate,
			AuthTokenGeneratorService authTokenGeneratorService) {
		super(restTemplate);
		this.baseUrl = System.getenv(XtaasConstants.DIALER_XTAAS_BASE_URL);
		// this.authorization = System.getenv(XtaasConstants.DIALER_AUTHORIZATION);
		this.authTokenGeneratorService = authTokenGeneratorService;
	}

	public <T> T get(String resourcePath, ParameterizedTypeReference<T> responseType) {
		URI uri = buildUri(resourcePath);
		return super.get(uri, securityHeaders(), responseType);
	}

	public <T> List<T> getAsList(String resourcePath, ParameterizedTypeReference<T[]> responseType) {
		T[] result = get(resourcePath, responseType);
		return result == null ? Arrays.asList() : Arrays.asList(result);
	}

	public <T, R> T post(String resourcePath, ParameterizedTypeReference<T> responseType, R jsonObj) {
		Gson gson = new Gson();
		String jsonBody = gson.toJson(jsonObj);
		URI uri = buildUri(resourcePath);
		return super.post(uri, securityHeaders(), responseType, jsonBody);
	}

	public <T, R> T put(String resourcePath, ParameterizedTypeReference<T> responseType, R jsonObj) {
		Gson gson = new Gson();
		String jsonBody = gson.toJson(jsonObj);
		URI uri = buildUri(resourcePath);
		return super.put(uri, securityHeaders(), responseType, jsonBody);
	}

	public <T> T delete(String resourcePath, ParameterizedTypeReference<T> responseType) {
		URI uri = buildUri(resourcePath);
		return super.get(uri, securityHeaders(), responseType);
	}

	private HttpHeaders securityHeaders() {
		HttpHeaders headers = new HttpHeaders();

		headers.add("accept", "application/json");
		headers.add("content-type", "application/json");
		headers.add("Authorization", generateAuthHeader());
		return headers;
	}

	private URI buildUri(String resourcePath) {
		return UriComponentsBuilder.fromUriString(baseUrl + resourcePath).build(true).toUri();
	}

	private String generateAuthHeader() {
		return authTokenGeneratorService.generateSystemToken(App.DIALER.name(), "dialer");
	}
}
