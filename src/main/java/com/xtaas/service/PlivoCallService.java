package com.xtaas.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.plivo.api.exceptions.PlivoRestException;
import com.plivo.api.models.call.Call;
import com.plivo.api.models.call.CallCreator;
import com.plivo.api.models.call.LiveCall;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.service.dto.CampaignDTO;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.util.MCrypt;
import com.xtaas.service.util.XtaasConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.HashSet;

@Service
public class PlivoCallService {

	private final Logger log = LoggerFactory.getLogger(PlivoCallService.class);
	private final long ringTimeout;
	private final String thinQDomainUrl;
	private final String thinQId;
	private final String thinQToken;
	private final String telnyxDomainUrl;
	private final String telnyxToken;
	private final String plivoAppUrl;
	private final String plivoAppHangupUrl;
	private final String plivoAppFallbackUrl;
	private final CacheService cacheService;
	private final String domainViva;
	private final String accountIdViva;
	private final String passcodeViva;
	private final MCrypt mCrypt;

	public PlivoCallService(CacheService cacheService) {
		this.cacheService = cacheService;
		this.ringTimeout = parseLong(System.getenv(XtaasConstants.TWILIO_RING_TIMEOUT));
		this.thinQDomainUrl = System.getenv(XtaasConstants.THINQ_DOMAIN_URL);
		this.thinQId = System.getenv(XtaasConstants.THINQ_ID);
		this.thinQToken = System.getenv(XtaasConstants.THINQ_TOKEN);
		this.telnyxDomainUrl = System.getenv(XtaasConstants.TELNYX_DOMAIN_URL);
		this.telnyxToken = System.getenv(XtaasConstants.TELNYX_TOKEN);
		this.plivoAppUrl = System.getenv(XtaasConstants.PLIVO_APPURL);
		this.plivoAppHangupUrl = System.getenv(XtaasConstants.PLIVO_APP_HANGUP_URL);
		this.plivoAppFallbackUrl = System.getenv(XtaasConstants.PLIVO_APP_FALLBACK_URL);
		this.domainViva = System.getenv(XtaasConstants.VIVA_DOMAIN);
		this.accountIdViva = System.getenv(XtaasConstants.VIVA_ACCOUNTID);
		this.passcodeViva = System.getenv(XtaasConstants.VIVA_PASSCODE);
		this.mCrypt = new MCrypt();
	}

	private long parseLong(String value) {
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			return 0;
		}
	}

	private String encryptStringForVIVA(String strToEncrypt, String secretKey) {
		String hexencrypt = null;
		try {
			hexencrypt = mCrypt.bytesToHex(mCrypt.encrypt(strToEncrypt, secretKey));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hexencrypt;
	}

	public String placeCall(ProspectCallDTO prospectCallDTO, String fromNumber, String answerUrl, CampaignDTO campaignDTO)
			throws URISyntaxException {
		String callSid = null;
		String pcid = prospectCallDTO.getProspectCallId();
		String toNumber = prospectCallDTO.getProspect().getPhone();
		/*
		 * AMD on dataSlice, callRetryCount > 0 & enableMachineAnsweredDetection flags
		 */
		String dataSlice = prospectCallDTO.getDataSlice();
		int callRetryCount = prospectCallDTO.getCallRetryCount();
		// boolean directPhone = prospectCallDTO.getProspect().isDirectPhone();
		boolean useSlice = campaignDTO.isUseSlice();
		boolean disableAMDOnQueued = campaignDTO.isDisableAMDOnQueued();
		boolean enableMachineAnsweredDetection = campaignDTO.isEnableMachineAnsweredDetection();
		try {
			Long callTimeLimit = cacheService
					.getLongApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.PLIVO_MAX_CALL_TIME_LIMIT.name(), 1800);
			Long maxCallRingingDuration = cacheService
					.getLongApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION_AUTO_DIAL.name(), 60);
			Map<String, String> sipHeaders = new HashMap<String, String>();
			if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("ThinQ")) {
				toNumber = createThinQNumber(toNumber, prospectCallDTO.getProspect().getCountry());
				sipHeaders.put("thinQid", thinQId);
				sipHeaders.put("thinQtoken", thinQToken);
			} else if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("Telnyx")) {
				toNumber = createTelnyxNumber(toNumber, prospectCallDTO.getProspect().getCountry());
				sipHeaders.put("Telnyx-Token", telnyxToken);
			} else if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("Viva")) {
				String secretKey = mCrypt.randomAlphaNumeric(16);
				String encryptedAccountIdString = null;
				encryptedAccountIdString = encryptStringForVIVA(accountIdViva, secretKey);
				String encryptedPasscodeString = null;
				encryptedPasscodeString = encryptStringForVIVA(passcodeViva, secretKey);
				if (encryptedPasscodeString != null && encryptedAccountIdString != null) {
					sipHeaders.put("VIVA_ACCOUNTID", encryptedAccountIdString);
					sipHeaders.put("VIVA_PASSCODE", encryptedPasscodeString);
					sipHeaders.put("VIVA_KEY", secretKey);
					sipHeaders.put("DIAL_TYPE", "AUTO");
				}
				toNumber = createVivaNumber(toNumber, prospectCallDTO.getProspect().getCountry());
			} else {
				String countryName = prospectCallDTO.getProspect().getCountry();
				if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
						|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
						|| countryName.equalsIgnoreCase("CA"))) {
					toNumber = getSimpleFormattedUSPhoneNumber(toNumber);
				}
			}
			CallCreator callCreator = null;
			if (enableMachineAnsweredDetection && useSlice && disableAMDOnQueued && callRetryCount > 0 && dataSlice != null
					&& !dataSlice.equalsIgnoreCase("slice0") && !dataSlice.equalsIgnoreCase("slice10")) {
				/*
				 * AMD on data which is not in slice0, slice10 & Queued --
				 * enableMachineAnsweredDetection is true, useSlice is true, disableAMDOnQueued
				 * is true, callRetryCount > 0 (i.e. 2nd retry) & dataSlice is slice1
				 */
				callCreator = plivoInitiateAMDCall(callCreator, pcid, fromNumber, toNumber, answerUrl, callTimeLimit,
						campaignDTO, sipHeaders);
			} else if (enableMachineAnsweredDetection && useSlice && !disableAMDOnQueued && dataSlice != null
					&& !dataSlice.equalsIgnoreCase("slice0") && !dataSlice.equalsIgnoreCase("slice10")) {
				/*
				 * AMD on data which is not in slice0 & slice10 --
				 * enableMachineAnsweredDetection is true, useSlice is true, disableAMDOnQueued
				 * is false & dataSlice is slice1
				 */
				callCreator = plivoInitiateAMDCall(callCreator, pcid, fromNumber, toNumber, answerUrl, callTimeLimit,
						campaignDTO, sipHeaders);
			} else if (enableMachineAnsweredDetection && !useSlice) {
				/*
				 * AMD on all calls -- enableMachineAnsweredDetection is true & useSlice is
				 * false
				 */
				callCreator = plivoInitiateAMDCall(callCreator, pcid, fromNumber, toNumber, answerUrl, callTimeLimit,
						campaignDTO, sipHeaders);
			} else {
				// NO AMD
				callCreator = Call.creator(fromNumber, Collections.singletonList(toNumber), answerUrl).ringTimeout(maxCallRingingDuration)
						.answerMethod("POST").hangupUrl(plivoAppHangupUrl).hangupMethod("POST").fallbackUrl(plivoAppFallbackUrl)
						.sipHeaders(sipHeaders).timeLimit(callTimeLimit);
			}
			callSid = callCreator.create().getRequestUuid();
		} catch (PlivoRestException | IOException e) {
			log.error(new SplunkLoggingUtils("PlivoCallError", prospectCallDTO.getProspectCallId())
				.eventDescription("Exception occurred in Plivo placeCall for pcid " + prospectCallDTO.getProspectCallId())
				.addThrowableWithStacktrace(e).build());
		}
		return callSid;
	}


	private CallCreator plivoInitiateAMDCall(CallCreator callCreator, String pcid, String fromNumber, String toNumber,
			String answerUrl, Long callTimeLimit, CampaignDTO campaignDTO, Map<String, String> sipHeaders) {
		/**
		 * In case of Sync AMD Plivo calls HangupURL & in case of Async AMD Plivo calls
		 * URL passed to machineDetectionUrl method.
		 */
		String plivoAMDType = cacheService
				.getApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.PLIVO_AMD_TYPE.name(), "ASYNC");
		// Long plivoAMDTime = cacheService.getLongApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.PLIVO_AMD_TIME.name(), 5000L);
		Long plivoAMDTime = campaignDTO.getPlivoAMDTime();
		if (plivoAMDTime == null || (plivoAMDTime != null && plivoAMDTime < 2000L && plivoAMDTime > 10000L)) {
			plivoAMDTime = 5000L; // default value for machine detection time
		}
		if (plivoAMDType.equalsIgnoreCase("SYNC")) {
			log.info("Plivo initiating call for [{}] using Sync AMD with plivoAMDTime : " + plivoAMDTime, toNumber);
			String plivoSyncMachineDetectionUrl = plivoAppHangupUrl + "?pcid=" + pcid + "&MachineAnsweredDetection="
					+ campaignDTO.isEnableMachineAnsweredDetection();
			callCreator = Call.creator(fromNumber, Collections.singletonList(toNumber), answerUrl).ringTimeout(ringTimeout)
					.answerMethod("POST").hangupUrl(plivoSyncMachineDetectionUrl).hangupMethod("POST")
					.fallbackUrl(plivoAppFallbackUrl).machineDetection("hangup").machineDetectionTime(plivoAMDTime)
					.sipHeaders(sipHeaders).timeLimit(callTimeLimit);
		} else {
			log.info("Plivo initiating call for [{}] using Async AMD with plivoAMDTime : " + plivoAMDTime, toNumber);
			String plivoAsyncMachineDetectionUrl = plivoAppUrl + "?pcid=" + pcid + "&MachineAnsweredDetection="
					+ campaignDTO.isEnableMachineAnsweredDetection() + "&AutoMachineHangup=" + campaignDTO.isAutoMachineHangup();
			if (campaignDTO.isAutoMachineHangup()) {
				callCreator = Call.creator(fromNumber, Collections.singletonList(toNumber), answerUrl).ringTimeout(ringTimeout)
						.answerMethod("POST").hangupUrl(plivoAppHangupUrl).hangupMethod("POST").fallbackUrl(plivoAppFallbackUrl)
						.machineDetection("hangup").machineDetectionTime(plivoAMDTime)
						.machineDetectionUrl(plivoAsyncMachineDetectionUrl).machineDetectionMethod("POST").sipHeaders(sipHeaders)
						.timeLimit(callTimeLimit);
			} else {
				callCreator = Call.creator(fromNumber, Collections.singletonList(toNumber), answerUrl).ringTimeout(ringTimeout)
						.answerMethod("POST").hangupUrl(plivoAppHangupUrl).hangupMethod("POST").fallbackUrl(plivoAppFallbackUrl)
						.machineDetection("true").machineDetectionTime(plivoAMDTime).machineDetectionUrl(plivoAsyncMachineDetectionUrl)
						.machineDetectionMethod("POST").sipHeaders(sipHeaders).timeLimit(callTimeLimit);
			}
		}
		return callCreator;
	}

	private String createThinQNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber;
		}
		StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@").append(thinQDomainUrl);
		return thinQNumber.toString();
	}

	private String createTelnyxNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber;
		}
		StringBuilder telnyxNumber = new StringBuilder("sip:").append(toNumber).append("@").append(telnyxDomainUrl);
		return telnyxNumber.toString();
	}

	private static String getSimpleFormattedUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d.]", ""); // removes all non-numeric chars
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = "+1" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

	private String createVivaNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedVivaUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			// toNumber = "+" + toNumber;
		}
		StringBuilder vivaNumber = new StringBuilder("sip:").append(toNumber).append("@").append(domainViva);
		return vivaNumber.toString();
	}

	private String getSimpleFormattedVivaUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		// removes all non-numeric chars
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = "1" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

	public String getPlivoCallStatus(String callSid) {
		String callStatus = "";
		try {
			callStatus = LiveCall.getter(callSid).get().getCallStatus().toString();
		} catch (PlivoRestException | IOException e) {
			log.error("PlivoCallService - Exception occurred in getPlivoCallStatus.");
			log.error("Exception is : {} ", e.getMessage());
		}
		return callStatus;
	}

	public void cancelPlivoCall(String callSid) {
		try {
			Call.deleter(callSid).delete();
			log.info("Call {} deleted successfully.", callSid);
		} catch (PlivoRestException | IOException e) {
			log.error("PlivoCallService - Exception occurred in cancelPlivoCall.");
			log.error("Exception is : {} ", e.getMessage());
		}
	}

}
