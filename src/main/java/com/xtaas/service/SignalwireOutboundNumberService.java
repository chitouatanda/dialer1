package com.xtaas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import com.xtaas.domain.SignalwireOutboundNumber;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.util.PhoneNumberUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
public class SignalwireOutboundNumberService {    

	private final Logger logger = LoggerFactory.getLogger(SignalwireOutboundNumberService.class);
	public static final String OUTBOUND_NUMBER_URL_TEMPLATE = "/spr/signalwire/outboundnumbers";
	private final XTaaSService xtaasService;
	private Map<String, List<SignalwireOutboundNumber>> signalwireOutboundNumbersMap = new HashMap<>();
	private Map<String, Integer> signalwirePoolMap = new HashMap<>();

	public SignalwireOutboundNumberService(XTaaSService xtaasService) {
		this.xtaasService = xtaasService;
	}

	public Boolean refreshOutboundNumbers() {
		if (signalwireOutboundNumbersMap != null) {
			signalwireOutboundNumbersMap.clear();
		}
		logger.info("Signalwire outbound number cache refreshed successully.");
		return true;
	}

	public String getSignalwireOutboundNumber(ProspectCallDTO prospectCallDTO, boolean localOutboundCalling,
			String organizationId) {
		List<SignalwireOutboundNumber> outboundNumbers = signalwireOutboundNumbersMap.get(organizationId);
		if (outboundNumbers == null || outboundNumbers.isEmpty()) {
			outboundNumbers = signalwireOutboundNumbersMap.get("XTAAS CALL CENTER");
			if (outboundNumbers == null || outboundNumbers.isEmpty()) {
				cacheAllSignalwireOutboundNumber();
				outboundNumbers = signalwireOutboundNumbersMap.get(organizationId);
				if (outboundNumbers == null || outboundNumbers.isEmpty()) {
					outboundNumbers = signalwireOutboundNumbersMap.get("XTAAS CALL CENTER");
					signalwireOutboundNumbersMap.put(organizationId, outboundNumbers);
				}
				cacheSignalwireNumbersPoolDetails(outboundNumbers);
			}
		}
		String number = getOutboundNumber(prospectCallDTO, localOutboundCalling, organizationId, outboundNumbers,
				signalwirePoolMap);
		return number;
	}

	private String getOutboundNumber(ProspectCallDTO prospectCallDTO, boolean localOutboundCalling,
			String organizationId, List<SignalwireOutboundNumber> outboundNumbers, Map<String, Integer> poolMap) {
		List<SignalwireOutboundNumber> outboundNumberList = new ArrayList<>();
		String phoneNumber = prospectCallDTO.getProspect().getPhone().replaceAll("[^\\d]", "");
		String countryName = prospectCallDTO.getProspect().getCountry();
		int maxPoolSize = 2;
		String key = organizationId + "_" + countryName;
		if (countryName != null && poolMap.get(key) != null) {
			maxPoolSize = poolMap.get(key);
		}
		int tempMaxPool = maxPoolSize;
		int pool = prospectCallDTO.getCallRetryCount() % maxPoolSize;
		int areaCode = Integer.parseInt(PhoneNumberUtil.findAreaCode(phoneNumber));
		if (pool == 0) {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
						&& number.getCountryName().equalsIgnoreCase(countryName)).collect(Collectors.toList());
			}
		} else {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == pool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(
						number -> number.getPool() == pool && number.getCountryName().equalsIgnoreCase(countryName))
						.collect(Collectors.toList());
			}
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers.stream()
					.filter(number -> number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers;
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = signalwireOutboundNumbersMap.get("XTAAS CALL CENTER");
		}

		SignalwireOutboundNumber outboundNumber = outboundNumberList.get(new Random().nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	private void cacheAllSignalwireOutboundNumber() {
		logger.info("cacheAllSignalwireOutboundNumber() :: Fetching signalwireoutboundnumbers from DB to cache.");
		List<SignalwireOutboundNumber> outboundNumbers = xtaasService.getAsList(OUTBOUND_NUMBER_URL_TEMPLATE,
				new ParameterizedTypeReference<SignalwireOutboundNumber[]>() {
				});
		signalwireOutboundNumbersMap = outboundNumbers.stream()
				.filter(number -> number.getProvider() != null && number.getProvider().equalsIgnoreCase("SIGNALWIRE"))
				.collect(Collectors.groupingBy(SignalwireOutboundNumber::getPartnerId));
	}

	private void cacheSignalwireNumbersPoolDetails(List<SignalwireOutboundNumber> outboundNumbers) {
		for (SignalwireOutboundNumber outboundNumber : outboundNumbers) {
			String key = outboundNumber.getPartnerId() + "_" + outboundNumber.getCountryName();
			if (signalwirePoolMap.get(key) == null) {
				signalwirePoolMap.put(key, outboundNumber.getPool());
			} else if (signalwirePoolMap.get(key) < new Integer(outboundNumber.getPool())) {
				signalwirePoolMap.put(key, outboundNumber.getPool());
			}
		}
	}
    
}
