package com.xtaas.service;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
public class AgentQueueService {
	public static final String ACTIVE_CAMPAIGN_URL = "/spr/api/agentqueue/campaigns";
	
	private final XTaaSService xtaasService;
	
	public AgentQueueService(XTaaSService xtaasService) {
		this.xtaasService = xtaasService;
	}
	
	public List<String> getActiveCampaigns() {
		return xtaasService.get(ACTIVE_CAMPAIGN_URL, new ParameterizedTypeReference<List<String>>(){});
	}
}
