package com.xtaas.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import com.xtaas.service.dto.ApplicationPropertyDTO;

@Service
public class CacheService {

	private final Logger log = LoggerFactory.getLogger(CacheService.class);
	private final XTaaSService xtaasService;

	private Map<String, String> applicationPropertyCache = new HashMap<>();
	private static final String APPLICATION_PROPERTY_URL_TEMPLATE = "/spr/applicationproperties";

	public CacheService(XTaaSService xtaasService) {
		this.xtaasService = xtaasService;
	}

	public Boolean loadApplicationPropertyCacheForcefully() {
		log.info("########### REFRESHING APPLICATION PROPERTY CACHE ###########");
		List<ApplicationPropertyDTO> applicationPropertyList = xtaasService.getAsList(APPLICATION_PROPERTY_URL_TEMPLATE,
				new ParameterizedTypeReference<ApplicationPropertyDTO[]>() {
				});
		applicationPropertyCache = new HashMap<String, String>();
		for (ApplicationPropertyDTO property : applicationPropertyList) {
			applicationPropertyCache.put(property.getName(), property.getValue());
		}
		log.info("Loaded ApplicationProperty cache forcefully. Cache size: " + applicationPropertyCache.size());
		return true;
	}

	public String getApplicationPropertyValue(String propertyName) {
		if (applicationPropertyCache == null) {
			// double check incase it is already loaded by other thread.
			if (applicationPropertyCache == null) {
				log.info("getApplicationPropertyValue() : Loading ApplicationProperty cache.");
				loadApplicationPropertyCacheForcefully();
				log.info("getApplicationPropertyValue() : Loaded ApplicationProperty cache. Cache size: "
						+ applicationPropertyCache.size());
			}
		}
		return applicationPropertyCache.get(propertyName);
	}

	public String getApplicationPropertyValue(String propertyName, String defaultValue) {
		String value = getApplicationPropertyValue(propertyName);
		return (value == null) ? defaultValue : value;
	}

	public boolean getBooleanApplicationPropertyValue(String propertyName, boolean defaultValue) {
		String value = getApplicationPropertyValue(propertyName);
		return (value == null) ? defaultValue : Boolean.parseBoolean(value);
	}

	public int getIntApplicationPropertyValue(String propertyName, int defaultValue) {
		String value = getApplicationPropertyValue(propertyName);
		return (value == null) ? defaultValue : Integer.parseInt(value);
	}

	public long getLongApplicationPropertyValue(String propertyName, long defaultValue) {
		String value = getApplicationPropertyValue(propertyName);
		return (value == null) ? defaultValue : Long.parseLong(value);
	}

}
