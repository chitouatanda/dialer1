package com.xtaas.service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriUtils;

import com.google.gson.Gson;
import com.xtaas.service.dto.ProspectCallLogUpdateDTO;
import com.xtaas.service.dto.ProspectSearchDTO;
import com.xtaas.service.dto.QueuedProspectDTO;
import com.xtaas.valueobject.ProspectCallStatus;
import com.xtaas.valueobject.ProspectSearchClauses;

@Service
public class ProspectService {
	public static final String CALLABLE_PROSPECT_URL_TEMPLATE = "/spr/api/prospect?searchstring=%s";
	public static final String UPDATE_PROSPECT_CALLLOG = "/spr/api/prospectcalllog/update";
	public static final String ALLOCATE_AGENT_URL = "/spr/agent/allocate/dialer/%s/%s";
	private static final String CALL_DISCONNECT_URL_TEMPLATE = "/spr/api/call/%s/call_disconnect";

	private final XTaaSService xtaasService;

	public ProspectService(XTaaSService xtaasService) {
		this.xtaasService = xtaasService;
	}

	public QueuedProspectDTO getCallableProspects(String campaignId, int activeCallCount, String phoneType)
			throws UnsupportedEncodingException {
		ProspectSearchDTO prospectSearchDTO = new ProspectSearchDTO();
		prospectSearchDTO.setClause(ProspectSearchClauses.ByCampaign);
		prospectSearchDTO.setCampaignId(campaignId);
		prospectSearchDTO.setQueuedCallCount(activeCallCount);
		prospectSearchDTO.setPhoneType(phoneType);
		String callableProspectUrl = String.format(CALLABLE_PROSPECT_URL_TEMPLATE,
				UriUtils.encode((new Gson()).toJson(prospectSearchDTO), StandardCharsets.UTF_8.toString()));
		return xtaasService.get(callableProspectUrl, new ParameterizedTypeReference<QueuedProspectDTO>() {
		});
		// return xtaasService.get(callableProspectUrl, new
		// ParameterizedTypeReference<List<ProspectCallDTO>>(){});
	}

	public void setCallback(String prospectCallId, Date callbackDate) {
		ProspectCallLogUpdateDTO updateDTO = new ProspectCallLogUpdateDTO();
		updateDTO.setProspectCallId(prospectCallId);
		updateDTO.setCallbackDate(callbackDate);
		xtaasService.put(UPDATE_PROSPECT_CALLLOG, new ParameterizedTypeReference<String>() {
		}, updateDTO);
	}

	public void updateProspect(String prospectCallId, String prospectInteractionSessionId, String twilioCallSid,
			ProspectCallStatus prospectCallStatus, int callRetryCount, Long dailyCallRetryCount,
			String outboundNumber) {
		ProspectCallLogUpdateDTO updateDTO = new ProspectCallLogUpdateDTO();
		updateDTO.setProspectCallId(prospectCallId);
		updateDTO.setProspectInteractionSessionId(prospectInteractionSessionId);
		updateDTO.setTwilioCallSid(twilioCallSid);
		updateDTO.setProspectCallStatus(prospectCallStatus);
		updateDTO.setCallRetryCount(callRetryCount);
		updateDTO.setDailyCallRetryCount(dailyCallRetryCount);
		updateDTO.setOutboundNumber(outboundNumber);
		xtaasService.put(UPDATE_PROSPECT_CALLLOG, new ParameterizedTypeReference<String>() {
		}, updateDTO);
	}

	public void allocateAgentPreviewMode(String callSid, boolean isCallAbandon) {
		String allocateAgentUrl = String.format(ALLOCATE_AGENT_URL, callSid, isCallAbandon);
		xtaasService.get(allocateAgentUrl, new ParameterizedTypeReference<String>(){});
	}
	
	public String notifyCallDisconnectToAgent(String callSid) {
		String disconnectUrl = String.format(CALL_DISCONNECT_URL_TEMPLATE, callSid);
		return xtaasService.get(disconnectUrl, new ParameterizedTypeReference<String>() {});
	}

}
