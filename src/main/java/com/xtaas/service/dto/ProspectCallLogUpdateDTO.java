package com.xtaas.service.dto;

import java.util.Date;

import com.xtaas.valueobject.ProspectCallStatus;

public class ProspectCallLogUpdateDTO {

	private String prospectCallId;
	private ProspectCallStatus prospectCallStatus;
	private String twilioCallSid;
	private int callRetryCount;
	private Long dailyCallRetryCount;
	private String prospectInteractionSessionId;
	private Date callbackDate;
	private String outboundNumber;

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public ProspectCallStatus getProspectCallStatus() {
		return prospectCallStatus;
	}

	public void setProspectCallStatus(ProspectCallStatus prospectCallStatus) {
		this.prospectCallStatus = prospectCallStatus;
	}

	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	public int getCallRetryCount() {
		return callRetryCount;
	}

	public void setCallRetryCount(int callRetryCount) {
		this.callRetryCount = callRetryCount;
	}

	public Long getDailyCallRetryCount() {
		return dailyCallRetryCount;
	}

	public void setDailyCallRetryCount(Long dailyCallRetryCount) {
		this.dailyCallRetryCount = dailyCallRetryCount;
	}

	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	public void setProspectInteractionSessionId(String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	public Date getCallbackDate() {
		return callbackDate;
	}

	public void setCallbackDate(Date callbackDate) {
		this.callbackDate = callbackDate;
	}

	public String getOutboundNumber() {
		return outboundNumber;
	}

	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}

}