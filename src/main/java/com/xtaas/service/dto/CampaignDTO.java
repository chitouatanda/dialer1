package com.xtaas.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.valueobject.DialerMode;

public class CampaignDTO {
	private String id;
	private String name;
	private boolean useSlice;
	private boolean enableMachineAnsweredDetection;
	private boolean disableAMDOnQueued;
	private String bucketName;
	private String foreignCampaign;
	private boolean localOutboundCalling;
	private DialerMode dialerMode;
	private String organizationId;
	private String callControlProvider;
	private boolean autoMachineHangup;
	private int telnyxCallTimeLimitSeconds;	
	private long plivoAMDTime;
	private boolean callAbandon;
	private String autoModeCallProvider;
	private String manualModeCallProvider;
	private boolean usePlivoSIP;
	private boolean autoIVR;
	private String sipProvider;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public boolean isUseSlice() {
		return useSlice;
	}

	public void setUseSlice(boolean useSlice) {
		this.useSlice = useSlice;
	}

	public boolean isEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(boolean enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public boolean isDisableAMDOnQueued() {
		return disableAMDOnQueued;
	}

	public void setDisableAMDOnQueued(boolean disableAMDOnQueued) {
		this.disableAMDOnQueued = disableAMDOnQueued;
	}

	public String getForeignCampaign() {
		return foreignCampaign;
	}

	public void setForeignCampaign(String foreignCampaign) {
		this.foreignCampaign = foreignCampaign;
	}

	public boolean isLocalOutboundCalling() {
		return localOutboundCalling;
	}

	public void setLocalOutboundCalling(boolean localOutboundCalling) {
		this.localOutboundCalling = localOutboundCalling;
	}

	public DialerMode getDialerMode() {
		return dialerMode;
	}

	public void setDialerMode(DialerMode dialerMode) {
		this.dialerMode = dialerMode;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getCallControlProvider() {
		return callControlProvider;
	}

	public void setCallControlProvider(String callControlProvider) {
		this.callControlProvider = callControlProvider;
	}

	public boolean isAutoMachineHangup() {
		return autoMachineHangup;
	}

	public void setAutoMachineHangup(boolean autoMachineHangup) {
		this.autoMachineHangup = autoMachineHangup;
	}

	public int getTelnyxCallTimeLimitSeconds() {
		return telnyxCallTimeLimitSeconds;
	}

	public void setTelnyxCallTimeLimitSeconds(int telnyxCallTimeLimitSeconds) {
		this.telnyxCallTimeLimitSeconds = telnyxCallTimeLimitSeconds;
	}
	
	public long getPlivoAMDTime() {
		return plivoAMDTime;
	}

	public void setPlivoAMDTime(long plivoAMDTime) {
		this.plivoAMDTime = plivoAMDTime;
	}

	public boolean isCallAbandon() {
		return callAbandon;
	}

	public void setCallAbandon(boolean callAbandon) {
		this.callAbandon = callAbandon;
	}

	public String getAutoModeCallProvider() {
		return autoModeCallProvider;
	}

	public void setAutoModeCallProvider(String autoModeCallProvider) {
		this.autoModeCallProvider = autoModeCallProvider;
	}

	public String getManualModeCallProvider() {
		return manualModeCallProvider;
	}

	public void setManualModeCallProvider(String manualModeCallProvider) {
		this.manualModeCallProvider = manualModeCallProvider;
	}

	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}
	
	public boolean isAutoIVR() {
		return autoIVR;
	}

	public void setAutoIVR(boolean autoIVR) {
		this.autoIVR = autoIVR;
	}

	public boolean isUsePlivoSIP() {
		return usePlivoSIP;
	}

	public void setUsePlivoSIP(boolean usePlivoSIP) {
		this.usePlivoSIP = usePlivoSIP;
	}

	@Override
	public String toString() {
		return "CampaignDTO [autoIVR=" + autoIVR + ", autoMachineHangup=" + autoMachineHangup
				+ ", autoModeCallProvider=" + autoModeCallProvider + ", bucketName=" + bucketName + ", callAbandon="
				+ callAbandon + ", callControlProvider=" + callControlProvider + ", dialerMode=" + dialerMode
				+ ", disableAMDOnQueued=" + disableAMDOnQueued + ", enableMachineAnsweredDetection="
				+ enableMachineAnsweredDetection + ", foreignCampaign=" + foreignCampaign + ", id=" + id
				+ ", localOutboundCalling=" + localOutboundCalling + ", manualModeCallProvider="
				+ manualModeCallProvider + ", name=" + name + ", organizationId=" + organizationId + ", plivoAMDTime="
				+ plivoAMDTime + ", sipProvider=" + sipProvider + ", telnyxCallTimeLimitSeconds="
				+ telnyxCallTimeLimitSeconds + ", usePlivoSIP=" + usePlivoSIP + ", useSlice=" + useSlice + "]";
	}
		
}