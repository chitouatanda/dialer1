package com.xtaas.service.dto;

public class RecordingFilterDTO {

	private String startDate;
	private String endDate;
	private String dateCreated;
	private String callSid;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	@Override
	public String toString() {
		return "RecordingFilterDTO [startDate=" + startDate + ", endDate=" + endDate + ", dateCreated=" + dateCreated
				+ ", callSid=" + callSid + "]";
	}

}
