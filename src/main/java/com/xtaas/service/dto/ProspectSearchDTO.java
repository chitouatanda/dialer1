package com.xtaas.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.valueobject.ProspectSearchClauses;

public class ProspectSearchDTO {
	@JsonProperty
	private ProspectSearchClauses clause;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private int queuedCallCount; // count for prospect in queued and ringing state.
	@JsonProperty
	private String phoneType;

	public ProspectSearchClauses getClause() {
		return clause;
	}

	/**
	 * @return the campaign
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param clause the clause to set
	 */
	public void setClause(ProspectSearchClauses clause) {
		this.clause = clause;
	}

	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public int getQueuedCallCount() {
		return queuedCallCount;
	}

	public void setQueuedCallCount(int queuedCallCount) {
		this.queuedCallCount = queuedCallCount;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

}
