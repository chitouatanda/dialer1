package com.xtaas.service.dto;

public class CallInfoDTO {

	private String to;

	private String from;

	private String campaignId;

	private String dialerMode;

	private String conferenceId;

	private String callControllId;

	private boolean isVoiceMail;

	private boolean isHangup;

	private String pcid;

	private boolean machineAnsweredDetection;

	private String dialer;

	private String whisperMsg;

	private String callerId;

	private String agentType;

	private String agentId;

	private String nextCallCommand;

	private String callStatus;

	private String callSid;
	
	private boolean autoMachineHangup;

	public CallInfoDTO() {
		this.isVoiceMail = false;
		this.isHangup = false;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getDialerMode() {
		return dialerMode;
	}

	public void setDialerMode(String dialerMode) {
		this.dialerMode = dialerMode;
	}

	public String getConferenceId() {
		return conferenceId;
	}

	public void setConferenceId(String conferenceId) {
		this.conferenceId = conferenceId;
	}

	public String getCallControllId() {
		return callControllId;
	}

	public void setCallControllId(String callControllId) {
		this.callControllId = callControllId;
	}

	public boolean isVoiceMail() {
		return isVoiceMail;
	}

	public void setVoiceMail(boolean isVoiceMail) {
		this.isVoiceMail = isVoiceMail;
	}

	public boolean isHangup() {
		return isHangup;
	}

	public void setHangup(boolean isHangup) {
		this.isHangup = isHangup;
	}

	public String getPcid() {
		return pcid;
	}

	public void setPcid(String pcid) {
		this.pcid = pcid;
	}

	public boolean isMachineAnsweredDetection() {
		return machineAnsweredDetection;
	}

	public void setMachineAnsweredDetection(boolean machineAnsweredDetection) {
		this.machineAnsweredDetection = machineAnsweredDetection;
	}

	public String getDialer() {
		return dialer;
	}

	public void setDialer(String dialer) {
		this.dialer = dialer;
	}

	public String getWhisperMsg() {
		return whisperMsg;
	}

	public void setWhisperMsg(String whisperMsg) {
		this.whisperMsg = whisperMsg;
	}

	public String getCallerId() {
		return callerId;
	}

	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getNextCallCommand() {
		return nextCallCommand;
	}

	public void setNextCallCommand(String nextCallCommand) {
		this.nextCallCommand = nextCallCommand;
	}

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	public boolean isAutoMachineHangup() {
		return autoMachineHangup;
	}

	public void setAutoMachineHangup(boolean autoMachineHangup) {
		this.autoMachineHangup = autoMachineHangup;
	}

}