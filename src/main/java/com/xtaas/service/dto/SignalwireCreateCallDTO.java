package com.xtaas.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
    "To",
    "Url",
    "From",
    "StatusCallbackEvent",
    "StatusCallback",
    "Record"
    })

public class SignalwireCreateCallDTO {

    @JsonProperty("To")
    private String to;

    @JsonProperty("From")
	private String from;

	// private String ApplicationSid;

    @JsonProperty("Url")
    private String url;

    @JsonProperty("Record")
    private String record;

    @JsonProperty("StatusCallback")
    private String statusCallback;

    @JsonProperty("StatusCallbackEvent")
    private String statusCallbackEvent;

    // private String FallbackUrl;

    // private String MachineDetection;

    // private String MachineDetectionTimeout;

    public String getTo() {
        return this.to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String getStatusCallback() {
        return this.statusCallback;
    }

    public void setStatusCallback(String statusCallback) {
        this.statusCallback = statusCallback;
    }

    public String getStatusCallbackEvent() {
        return this.statusCallbackEvent;
    }

    public void setStatusCallbackEvent(String statusCallbackEvent) {
        this.statusCallbackEvent = statusCallbackEvent;
    }

    // public String getFallbackUrl() {
    //     return FallbackUrl;
    // }

    // public void setFallbackUrl(String fallbackUrl) {
    //     FallbackUrl = fallbackUrl;
    // }

    // public String getMachineDetection() {
    //     return MachineDetection;
    // }

    // public void setMachineDetection(String machineDetection) {
    //     MachineDetection = machineDetection;
    // }

    // public String getMachineDetectionTimeout() {
    //     return MachineDetectionTimeout;
    // }

    // public void setMachineDetectionTimeout(String machineDetectionTimeout) {
    //     MachineDetectionTimeout = machineDetectionTimeout;
    // }

    // public String getApplicationSid() {
    //     return ApplicationSid;
    // }

    // public void setApplicationSid(String applicationSid) {
    //     ApplicationSid = applicationSid;
    // }
    
}
