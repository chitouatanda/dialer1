package com.xtaas.service.dto;

import java.util.List;

public class QueuedProspectDTO {

	private List<ProspectCallDTO> queuedProspects;
	private int delta;

	protected QueuedProspectDTO() {
	}

	public QueuedProspectDTO(List<ProspectCallDTO> queuedProspects, int delta) {
		super();
		this.queuedProspects = queuedProspects;
		this.delta = delta;
	}

	public List<ProspectCallDTO> getQueuedProspects() {
		return queuedProspects;
	}

	public void setQueuedProspects(List<ProspectCallDTO> queuedProspects) {
		this.queuedProspects = queuedProspects;
	}

	public int getDelta() {
		return delta;
	}

	public void setDelta(int delta) {
		this.delta = delta;
	}

}