package com.xtaas.service.dto;

public class TelnyxCallStatusDTO {

  private String callControlId;

  private String status;

  public String getCallControlId() {
    return callControlId;
  }

  public void setCallControlId(String callControlId) {
    this.callControlId = callControlId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

}