package com.xtaas.service.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.Prospect;

public class ProspectCallDTO {
	@JsonProperty
	private String prospectCallId;
	@JsonProperty
	private Prospect prospect;
	@JsonProperty
	private String twilioCallSid;
	@JsonProperty
	private long callbackTime; // set the callback in MS
	@JsonProperty
	private String outboundNumber; // outbound number used to call this prospect
	@JsonProperty
	private int callRetryCount; // Number of times a prospect has been retried
	@JsonProperty
	private String prospectInteractionSessionId; // unique session id for the
	@JsonProperty
	private Long dailyCallRetryCount; // Number of calls made to a prospect in a
	@JsonProperty
	private List<IndirectProspectsDTO> indirectProspects;
	@JsonProperty
	private int prospectVersion;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String dataSlice;

	public ProspectCallDTO() {
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public Prospect getProspect() {
		return prospect;
	}

	public void setProspect(Prospect prospect) {
		this.prospect = prospect;
	}

	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	public long getCallbackTime() {
		return callbackTime;
	}

	public void setCallbackTime(long callbackTime) {
		this.callbackTime = callbackTime;
	}

	public String getOutboundNumber() {
		return outboundNumber;
	}

	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}

	public int getCallRetryCount() {
		return callRetryCount;
	}

	public void setCallRetryCount(int callRetryCount) {
		this.callRetryCount = callRetryCount;
	}

	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	public void setProspectInteractionSessionId(String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	public Long getDailyCallRetryCount() {
		return dailyCallRetryCount;
	}

	public void setDailyCallRetryCount(Long dailyCallRetryCount) {
		this.dailyCallRetryCount = dailyCallRetryCount;
	}

	public List<IndirectProspectsDTO> getIndirectProspects() {
		return indirectProspects;
	}

	public void setIndirectProspects(List<IndirectProspectsDTO> indirectProspects) {
		this.indirectProspects = indirectProspects;
	}

	public int getProspectVersion() {
		return prospectVersion;
	}

	public void setProspectVersion(int prospectVersion) {
		this.prospectVersion = prospectVersion;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

}
