package com.xtaas.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IndirectProspectsDTO {
	@JsonProperty
	private String firstName;
	@JsonProperty
	private String lastName;
	@JsonProperty
	private String title;
	@JsonProperty
	private String department;
	@JsonProperty
	private String company;
	@JsonProperty
	private String email;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String prospectCallId;
	@JsonProperty
	private String qualityBucket;
	@JsonProperty
	private int qualityBucketSort;
	@JsonProperty
	private String multiProspect;
	@JsonProperty
	private String status;

	public IndirectProspectsDTO() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getQualityBucket() {
		return qualityBucket;
	}

	public void setQualityBucket(String qualityBucket) {
		this.qualityBucket = qualityBucket;
	}

	public int getQualityBucketSort() {
		return qualityBucketSort;
	}

	public void setQualityBucketSort(int qualityBucketSort) {
		this.qualityBucketSort = qualityBucketSort;
	}

	public String getMultiProspect() {
		return multiProspect;
	}

	public void setMultiProspect(String multiProspect) {
		this.multiProspect = multiProspect;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

}