package com.xtaas.service;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class HttpService {
    private final Logger log = LoggerFactory.getLogger(HttpService.class);

    protected final RestTemplate restTemplate;

    public HttpService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public <T> T get(URI uri, HttpHeaders headers, ParameterizedTypeReference<T> responseType) {
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>("", headers),
                    responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("GET request Failed for '" + uri.toString() + "': " + ex.getResponseBodyAsString());
        }
        return null;
    }
    
    public <T, R> T post(URI uri, HttpHeaders headers, ParameterizedTypeReference<T> responseType, String jsonBody) {
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.POST,
                    new HttpEntity<>(jsonBody, headers), responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("POST request Failed for '" + uri.toString() + "' for JsonBody : '" + jsonBody + "' , with Exception: " + ex.getResponseBodyAsString());
        }
        return null;
    }

    public <T, R> T postUrlencoded(URI uri, HttpHeaders headers, ParameterizedTypeReference<T> responseType, MultiValueMap<String, String> map) {
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.POST,
                    new HttpEntity<>(map, headers), responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("POST urlencoded request Failed for '" + uri.toString() + "' for JsonBody : '" + map.toString() + "' , with Exception: " + ex.getResponseBodyAsString());
        }
        return null;
    }
    
    public <T, R> T put(URI uri, HttpHeaders headers, ParameterizedTypeReference<T> responseType, String jsonBody) {
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT,
                    new HttpEntity<>(jsonBody, headers), responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("PUT request Failed for '" + uri.toString() + "': " + ex.getResponseBodyAsString());
        }
        return null;
    }

    public <T> T delete(URI uri, HttpHeaders headers, ParameterizedTypeReference<T> responseType) {
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.DELETE,
                    new HttpEntity<>("", headers), responseType);

            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("DELETE request Failed for '" + uri.toString() + "': " + ex.getResponseBodyAsString());
        }
        return null;
    }

}