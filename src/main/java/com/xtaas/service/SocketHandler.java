package com.xtaas.service;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xtaas.service.util.XtaasConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class SocketHandler extends AbstractWebSocketHandler {

	private final Logger log = LoggerFactory.getLogger(SocketHandler.class);
	private Map<WebSocketSession, GoogleSpeechToTextService> sessions = new HashMap<>();
	private Map<WebSocketSession, String> sessionsCallSid = new HashMap<>();
	private List<String> finalTranscription = new ArrayList<>();
	// private String serverHandleVoiceCallUrl = "https://1a8e70c473db.ngrok.io/spr/signalwire/voice";
	private boolean isFinalLocal;
    private String serverBaseUrl;
    private HttpService httpService;

    public SocketHandler (HttpService httpService) {
        this.serverBaseUrl = System.getenv(XtaasConstants.DIALER_XTAAS_BASE_URL);
		this.httpService = httpService;
    }

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessions.put(session, new GoogleSpeechToTextService(session, transcriptionMap -> {
			String transcription = (String) transcriptionMap.get("transcription");
			// System.out.println("transcription map : " + transcriptionMap.toString());
			boolean isFinal = (boolean) transcriptionMap.get("isFinal");
			if (isFinal) {
				sendTranscription(session, transcription);
			}
		}));
	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map value = mapper.readValue(message.getPayload().toString(), Map.class);
			if (value.containsKey("event") && value.get("event").equals("start") && value.containsKey("start") && ((Map<String, String>) value.get("start")).containsKey("callSid")) {
				sessionsCallSid.put(session, ((Map<String, String>) value.get("start")).get("callSid"));
			}
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sessions.get(session).send(message.getPayload());
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessions.get(session).close();
		sessions.remove(session);
		// sessionsCallSid.remove(session);
		// finalTranscription.clear();
	}

	private HttpHeaders headers() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("accept", "*/*");
		headers.add("content-type", "application/json");
		headers.add("Authorization", "Basic YzQ5YTUxMWEtMmQ3YS00ODE2LWIxMzktMzAyYWU3ZDdjNzRkOlBUMTc2ZGE5Y2ZiMGRlYTk0ZGRlYTU0Njg1OWYyZjJlYjVlY2U4ZjYwN2Q3OTNiMThm");
		return headers;
	}

    private URI buildUri(String resourcePath) {
        return UriComponentsBuilder.fromUriString(resourcePath).build(true).toUri();
    }

	public void sendTranscription (WebSocketSession session, String transcription) {
		String callSid = sessionsCallSid.get(session);
		sessionsCallSid.remove(session);
		if (callSid != null && !callSid.isEmpty()) {
			// String serverBaseUrl = signalwireCallService.getBaseUrl();
			log.info("SocketHandler====> transcription : " + transcription);
           	String serverHandleVoiceCallUrl = serverBaseUrl + "/spr/signalwire/voice";
			//  String serverHandleVoiceCallUrl = "https://98d051319641.ngrok.io" + "/spr/signalwire/voice";
			String url = "https://xtaascorp.signalwire.com/api/laml/2010-04-01/Accounts/c49a511a-2d7a-4816-b139-302ae7d7c74d/Calls/" + callSid + ".json";
			String jsonBody = "{ \"Url\": \"" + serverHandleVoiceCallUrl + "?transcription="+ transcription +"\"}";
			httpService.post(buildUri(url), headers(), new ParameterizedTypeReference<Map<String, Object>>() {
			}, jsonBody);
		}		
	}
}
