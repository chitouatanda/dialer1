package com.xtaas.service;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import com.xtaas.service.dto.CampaignDTO;

@Service
public class CampaignService {
	public static final String CAMPAIGN_URL_TEMPLATE = "/spr/api/campaign/%s";
	
	private final XTaaSService xtaasService;
	
	public CampaignService(XTaaSService xtaasService) {
		this.xtaasService = xtaasService;
	}
	
	public CampaignDTO getCampaign(String campaignId) {
		String campaignUrl = String.format(CAMPAIGN_URL_TEMPLATE, campaignId);
		return xtaasService.get(campaignUrl, new ParameterizedTypeReference<CampaignDTO>(){});
	}
}
