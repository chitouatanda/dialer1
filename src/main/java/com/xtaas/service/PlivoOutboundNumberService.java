package com.xtaas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import com.xtaas.domain.PlivoOutboundNumber;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.util.PhoneNumberUtil;

@Service
public class PlivoOutboundNumberService {

	private final Logger logger = LoggerFactory.getLogger(PlivoOutboundNumberService.class);
	public static final String OUTBOUND_NUMBER_URL_TEMPLATE = "/spr/plivo/outboundnumbers";
	private final XTaaSService xtaasService;
	private Map<String, List<PlivoOutboundNumber>> plivoOutboundNumbersMap = new HashMap<>();
	private Map<String, Integer> plivoPoolMap = new HashMap<>();

	public PlivoOutboundNumberService(XTaaSService xtaasService) {
		this.xtaasService = xtaasService;
	}

	public Boolean refreshOutboundNumbers() {
		if (plivoOutboundNumbersMap != null) {
			plivoOutboundNumbersMap.clear();
		}
		logger.info("Plivo outbound number cache refreshed successully.");
		return true;
	}

	public String getPlivoOutboundNumber(ProspectCallDTO prospectCallDTO, boolean localOutboundCalling,
			String organizationId) {
		List<PlivoOutboundNumber> outboundNumbers = plivoOutboundNumbersMap.get(organizationId);
		if (outboundNumbers == null || outboundNumbers.isEmpty()) {
			outboundNumbers = plivoOutboundNumbersMap.get("XTAAS CALL CENTER");
			if (outboundNumbers == null || outboundNumbers.isEmpty()) {
				cacheAllPlivoOutboundNumber();
				outboundNumbers = plivoOutboundNumbersMap.get(organizationId);
				if (outboundNumbers == null || outboundNumbers.isEmpty()) {
					outboundNumbers = plivoOutboundNumbersMap.get("XTAAS CALL CENTER");
					plivoOutboundNumbersMap.put(organizationId, outboundNumbers);
				}
				cachePlivoNumbersPoolDetails(outboundNumbers);
			}
		}
		String number = getOutboundNumber(prospectCallDTO, localOutboundCalling, organizationId, outboundNumbers,
				plivoPoolMap);
		return number;
	}

	private String getOutboundNumber(ProspectCallDTO prospectCallDTO, boolean localOutboundCalling,
			String organizationId, List<PlivoOutboundNumber> outboundNumbers, Map<String, Integer> poolMap) {
		List<PlivoOutboundNumber> outboundNumberList = new ArrayList<>();
		String phoneNumber = prospectCallDTO.getProspect().getPhone().replaceAll("[^\\d]", "");
		String countryName = prospectCallDTO.getProspect().getCountry();
		int maxPoolSize = 2;
		String key = organizationId + "_" + countryName;
		if (countryName != null && poolMap.get(key) != null) {
			maxPoolSize = poolMap.get(key);
		}
		int tempMaxPool = maxPoolSize;
		int pool = prospectCallDTO.getCallRetryCount() % maxPoolSize;
		int areaCode = Integer.parseInt(PhoneNumberUtil.findAreaCode(phoneNumber));
		if (pool == 0) {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
						&& number.getCountryName().equalsIgnoreCase(countryName)).collect(Collectors.toList());
			}
		} else {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == pool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(
						number -> number.getPool() == pool && number.getCountryName().equalsIgnoreCase(countryName))
						.collect(Collectors.toList());
			}
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers.stream()
					.filter(number -> number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers;
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = plivoOutboundNumbersMap.get("XTAAS CALL CENTER");
		}

		PlivoOutboundNumber outboundNumber = outboundNumberList.get(new Random().nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	private void cacheAllPlivoOutboundNumber() {
		logger.info("cacheAllPlivoOutboundNumber() :: Fetching plivooutboundnumbers from DB to cache.");
		List<PlivoOutboundNumber> outboundNumbers = xtaasService.getAsList(OUTBOUND_NUMBER_URL_TEMPLATE,
				new ParameterizedTypeReference<PlivoOutboundNumber[]>() {
				});
		plivoOutboundNumbersMap = outboundNumbers.stream()
				.filter(number -> number.getProvider() != null && number.getProvider().equalsIgnoreCase("PLIVO"))
				.collect(Collectors.groupingBy(PlivoOutboundNumber::getPartnerId));
	}

	private void cachePlivoNumbersPoolDetails(List<PlivoOutboundNumber> outboundNumbers) {
		for (PlivoOutboundNumber outboundNumber : outboundNumbers) {
			String key = outboundNumber.getPartnerId() + "_" + outboundNumber.getCountryName();
			if (plivoPoolMap.get(key) == null) {
				plivoPoolMap.put(key, outboundNumber.getPool());
			} else if (plivoPoolMap.get(key) < new Integer(outboundNumber.getPool())) {
				plivoPoolMap.put(key, outboundNumber.getPool());
			}
		}
	}

}
