package com.xtaas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import com.xtaas.domain.OutboundNumber;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.util.PhoneNumberUtil;

@Service
public class OutboundNumberService {

	private final Logger logger = LoggerFactory.getLogger(OutboundNumberService.class);
	public static final String OUTBOUND_NUMBER_URL_TEMPLATE = "/spr/outboundnumbers";
	private final XTaaSService xtaasService;
	private Map<String, List<OutboundNumber>> twilioOutboundNumbersMap = new HashMap<>();
	private Map<String, Integer> twilioPoolMap = new HashMap<>();

	public OutboundNumberService(XTaaSService xtaasService) {
		this.xtaasService = xtaasService;
	}

	public Boolean refreshOutboundNumbers() {
		if (twilioOutboundNumbersMap != null) {
			twilioOutboundNumbersMap.clear();
		}
		logger.info("Twilio outbound number cache refreshed successully.");
		return true;
	}

	public String getTwilioOutboundNumber(ProspectCallDTO prospectCallDTO, boolean localOutboundCalling,
			String organizationId) {
		List<OutboundNumber> outboundNumbers = twilioOutboundNumbersMap.get(organizationId);
		if (outboundNumbers == null || outboundNumbers.isEmpty()) {
			cacheAllTwilioOutboundNumber();
			outboundNumbers = twilioOutboundNumbersMap.get(organizationId);
			if (outboundNumbers == null || outboundNumbers.isEmpty()) {
				outboundNumbers = twilioOutboundNumbersMap.get("XTAAS CALL CENTER");
				twilioOutboundNumbersMap.put(organizationId, outboundNumbers);
			}
			cacheTwilioNumbersPoolDetails(outboundNumbers);
		}
		String number = getOutboundNumber(prospectCallDTO, localOutboundCalling, organizationId, outboundNumbers,
				twilioPoolMap);
		return number;
	}

	private String getOutboundNumber(ProspectCallDTO prospectCallDTO, boolean localOutboundCalling,
			String organizationId, List<OutboundNumber> outboundNumbers, Map<String, Integer> poolMap) {
		List<OutboundNumber> outboundNumberList = new ArrayList<>();
		String phoneNumber = prospectCallDTO.getProspect().getPhone();
		String countryName = prospectCallDTO.getProspect().getCountry();
		int maxPoolSize = 2;
		String key = organizationId + "_" + countryName;
		if (countryName != null && poolMap.get(key) != null) {
			maxPoolSize = poolMap.get(key);
		}
		int tempMaxPool = maxPoolSize;
		int pool = prospectCallDTO.getCallRetryCount() % maxPoolSize;
		int areaCode = Integer.parseInt(PhoneNumberUtil.findAreaCode(phoneNumber));
		if (pool == 0) {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
						&& number.getCountryName().equalsIgnoreCase(countryName)).collect(Collectors.toList());
			}
		} else {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == pool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(
						number -> number.getPool() == pool && number.getCountryName().equalsIgnoreCase(countryName))
						.collect(Collectors.toList());
			}
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers.stream()
					.filter(number -> number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers;
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = twilioOutboundNumbersMap.get("XTAAS CALL CENTER");
		}

		OutboundNumber outboundNumber = outboundNumberList.get(new Random().nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	private void cacheAllTwilioOutboundNumber() {
		List<OutboundNumber> outboundNumbers = xtaasService.getAsList(OUTBOUND_NUMBER_URL_TEMPLATE,
				new ParameterizedTypeReference<OutboundNumber[]>() {
				});
		twilioOutboundNumbersMap = outboundNumbers.stream()
				.filter(number -> number.getProvider() != null && number.getProvider().equalsIgnoreCase("TWILIO"))
				.collect(Collectors.groupingBy(OutboundNumber::getPartnerId));
	}

	private void cacheTwilioNumbersPoolDetails(List<OutboundNumber> outboundNumbers) {
		for (OutboundNumber outboundNumber : outboundNumbers) {
			String key = outboundNumber.getPartnerId() + "_" + outboundNumber.getCountryName();
			if (twilioPoolMap.get(key) == null) {
				twilioPoolMap.put(key, outboundNumber.getPool());
			} else if (twilioPoolMap.get(key) < new Integer(outboundNumber.getPool())) {
				twilioPoolMap.put(key, outboundNumber.getPool());
			}
		}
	}

}
