package com.xtaas.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.google.api.gax.rpc.ClientStream;
import com.google.api.gax.rpc.ResponseObserver;
import com.google.api.gax.rpc.StreamController;
// import com.google.cloud.speech.v1.*;
import com.google.cloud.speech.v1p1beta1.*;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.WebSocketSession;

/**
 * Client class for Google Cloud Speech-to-Text
 *
 * This class relies on having your Google Credentials in a file named by the
 * GOOGLE_APPLICATION_CREDENTIALS environment variable
 *
 * Based on:
 * https://cloud.google.com/speech-to-text/docs/streaming-recognize#speech-streaming-recognize-java
 */
public class GoogleSpeechToTextService {

    final static Logger logger = LoggerFactory.getLogger(GoogleSpeechToTextService.class);
    ClientStream<StreamingRecognizeRequest> clientStream;
    ResponseObserver<StreamingRecognizeResponse> responseObserver;

    public GoogleSpeechToTextService(WebSocketSession session, Consumer<Map<String, Object>> onTranscription) throws IOException {
        SpeechClient client = SpeechClient.create();
        Map<String, Object> transcriptionDetails = new HashMap<>();
        responseObserver = new ResponseObserver<StreamingRecognizeResponse>() {
            @Override
            public void onStart(StreamController streamController) {
                logger.info("Started");
            }

            @Override
            public void onResponse(StreamingRecognizeResponse streamingRecognizeResponse) {
            	if (streamingRecognizeResponse.getResultsList().size() > 0) {
            		StreamingRecognitionResult result = streamingRecognizeResponse.getResultsList().get(0);
                    // logger.info("result obj: " + result.toString() + "\n");
                    if (result.getAlternativesList().size() > 0) {
                    	SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
                    	// logger.info("result alternative list : " + result.getAlternativesList().toString() + "\n");
                    	// logger.info("alternative obj: " + alternative.toString() + "\n");
                        transcriptionDetails.put("transcription", alternative.getTranscript());
                        transcriptionDetails.put("isFinal", result.getIsFinal());
                         if (result.getIsFinal()) {
                             logger.info("streamingRecognizeResponse obj: " + streamingRecognizeResponse.toString() + "\n");
                         }
                        onTranscription.accept(transcriptionDetails);
                        // onTranscription.accept(alternative.getTranscript());
                        // isFinal.accept(result.getIsFinal());	
                    }
            	}                
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error("Error on recognize request: {}", throwable);
            }

            @Override
            public void onComplete() {
                logger.info("Completed");
            }
        };

        clientStream = client.streamingRecognizeCallable().splitCall(responseObserver);

        RecognitionConfig recognitionConfig = RecognitionConfig.newBuilder()
                .setEncoding(RecognitionConfig.AudioEncoding.MULAW).setLanguageCode("en-US").setSampleRateHertz(8000)
                .build();
        StreamingRecognitionConfig streamingRecognitionConfig = StreamingRecognitionConfig.newBuilder()
                .setConfig(recognitionConfig).setInterimResults(true).build();

        StreamingRecognizeRequest request = StreamingRecognizeRequest.newBuilder()
                .setStreamingConfig(streamingRecognitionConfig).build(); // The first request in a streaming call has to
                                                                         // be a config

        clientStream.send(request);
    }

    public void send(String message) {
        JSONObject jo = new JSONObject(message);
        if (!jo.getString("event").equals("media")) {
            return;
        }

        String payload = jo.getJSONObject("media").getString("payload");
        byte[] data = Base64.getDecoder().decode(payload);
        StreamingRecognizeRequest request = StreamingRecognizeRequest.newBuilder()
                .setAudioContent(ByteString.copyFrom(data)).build();
        clientStream.send(request);

    }

    public void close() {
        logger.info("Closed");
        responseObserver.onComplete();
    }

}
