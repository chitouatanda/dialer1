package com.xtaas.service;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriUtils;

import com.xtaas.config.Constants;
import com.xtaas.domain.Prospect;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.service.dto.CampaignDTO;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.dto.TelnyxCallStatusDTO;
import com.xtaas.service.util.XtaasConstants;
import com.xtaas.valueobject.DialerMode;
import com.xtaas.valueobject.PlivoHangupCause;
import com.xtaas.valueobject.PlivoStatusConstants;
import com.xtaas.valueobject.ProspectCallStatus;
import com.xtaas.valueobject.TwilioStatusConstants;

@Service
public class CallService {
	private final Logger log = LoggerFactory.getLogger(CallService.class);

	private static final String REGISTER_CALL_URL_TEMPLATE = "/spr/api/call/%s/prospectCall";
	private final String twilioAppUrl;
	private final String plivoAppUrl;
	private final OutboundNumberService outboundNumberService;
	private final PlivoOutboundNumberService plivoOutboundNumberService;
	private final TwilioCallService twilioCallService;
	private final PlivoCallService plivoCallService;
	private final XTaaSService xtaasService;
	private final ProspectService prospectService;
	private Map<String, String> callStatusMap = new HashMap<>();
	private Map<String, String> plivoCallStatusMap = new HashMap<>();
	private ConcurrentHashMap<String, String> telnyxCallLegIdMap = new ConcurrentHashMap<>(); 
	private final TelnyxOutboundNumberService telnyxOutboundNumberService;
	private final TelnyxCallService telnyxCallService;
	private Map<String, TelnyxCallStatusDTO> telnyxCallControlStatusMap = new HashMap<>();
	private final String xtaasBaseUrl;
	private final SignalwireCallService signalwireCallService;
	private ConcurrentHashMap<String, String> signalwireCallStatusMap = new ConcurrentHashMap<>();
	private final SignalwireOutboundNumberService signalwireOutboundNumberService;

	public CallService(OutboundNumberService outboundNumberService,
			PlivoOutboundNumberService plivoOutboundNumberService, TelnyxOutboundNumberService telnyxOutboundNumberService,
			TwilioCallService twilioCallService, PlivoCallService plivoCallService, TelnyxCallService telnyxCallService,
			XTaaSService xtaasService, ProspectService prospectService, SignalwireCallService signalwireCallService, SignalwireOutboundNumberService signalwireOutboundNumberService) {
		this.twilioAppUrl = System.getenv(XtaasConstants.TWILIO_APP_URL);
		this.plivoAppUrl = System.getenv(XtaasConstants.PLIVO_APPURL);
		this.outboundNumberService = outboundNumberService;
		this.plivoOutboundNumberService = plivoOutboundNumberService;
		this.twilioCallService = twilioCallService;
		this.plivoCallService = plivoCallService;
		this.xtaasService = xtaasService;
		this.prospectService = prospectService;
		this.telnyxOutboundNumberService = telnyxOutboundNumberService;
		this.telnyxCallService = telnyxCallService;
		this.xtaasBaseUrl = System.getenv(XtaasConstants.DIALER_XTAAS_BASE_URL);
		this.signalwireCallService = signalwireCallService;
		this.signalwireOutboundNumberService = signalwireOutboundNumberService;
	}

	public ProspectCallStatus processProspectCall(ProspectCallDTO prospectCallDTO, CampaignDTO campaignDTO)
			throws UnsupportedEncodingException, URISyntaxException {
		Prospect prospect = prospectCallDTO.getProspect();
		String outboundNumber = "";
		String callProvider = null;
		if(campaignDTO.getAutoModeCallProvider()!=null && !campaignDTO.getAutoModeCallProvider().isEmpty()) {
			callProvider = campaignDTO.getAutoModeCallProvider();
		}else {
			callProvider = campaignDTO.getCallControlProvider();
		}
		
		if (callProvider != null && callProvider.equalsIgnoreCase("Plivo")) {
			outboundNumber = plivoOutboundNumberService.getPlivoOutboundNumber(prospectCallDTO,
					campaignDTO.isLocalOutboundCalling(), campaignDTO.getOrganizationId());
		} else if (callProvider != null && callProvider.equalsIgnoreCase("Telnyx")) {
			outboundNumber = telnyxOutboundNumberService.getTelnyxOutboundNumber(prospectCallDTO,
					campaignDTO.isLocalOutboundCalling(), campaignDTO.getOrganizationId());
		} else if (campaignDTO.getCallControlProvider() != null && campaignDTO.getCallControlProvider().equalsIgnoreCase("Signalwire")) {
			outboundNumber = signalwireOutboundNumberService.getSignalwireOutboundNumber(prospectCallDTO,
					campaignDTO.isLocalOutboundCalling(), campaignDTO.getOrganizationId());
			// if (outboundNumber == null || outboundNumber.isEmpty()) {
			// 	outboundNumber = "+13154967412";
			// }
		} else {
			outboundNumber = outboundNumberService.getTwilioOutboundNumber(prospectCallDTO,
					campaignDTO.isLocalOutboundCalling(), campaignDTO.getOrganizationId());
		}
		String answerUrl = "";
		StringBuilder answerUrlBuilder = null;
		if (callProvider.equalsIgnoreCase("Plivo")) {
			answerUrlBuilder = new StringBuilder(plivoAppUrl);
		} else if (campaignDTO.getCallControlProvider().equalsIgnoreCase("Signalwire")) {
			answerUrlBuilder = new StringBuilder(xtaasBaseUrl + "/spr/signalwire/voice");
		} else {
			answerUrlBuilder = new StringBuilder(twilioAppUrl);
		}
		answerUrl = answerUrlBuilder.append("?dialer=1&pcid=").append(prospectCallDTO.getProspectCallId())
				.append("&whisperMsg=")
				.append(
						UriUtils.encode(prospect.getFirstName() + " " + prospect.getLastName(), StandardCharsets.UTF_8.toString()))
				.append("&callerId=").append(UriUtils.encode(outboundNumber, StandardCharsets.UTF_8.toString()))
				.append("&MachineAnsweredDetection=").append(campaignDTO.isEnableMachineAnsweredDetection())
				.append("&AutoMachineHangup=").append(campaignDTO.isAutoMachineHangup()).toString();

		String callSid = null;
		String randomUUID = UUID.randomUUID().toString();
		if ((campaignDTO.getDialerMode().equals(DialerMode.POWER) || campaignDTO.getDialerMode().equals(DialerMode.HYBRID)
				|| campaignDTO.getDialerMode().equals(DialerMode.RESEARCH_AND_DIAL))
				&& prospect.getProspectType().equalsIgnoreCase(Constants.PROSPECT_TYPE.POWER.name())) {
			if (callProvider.equalsIgnoreCase("Plivo")) {
				log.info("Plivo - Initiating call for campaign [{}] and pcid [{}]", campaignDTO.getId(),
						prospectCallDTO.getProspectCallId());
				Date startDate = new Date();
				callSid = plivoCallService.placeCall(prospectCallDTO, outboundNumber, answerUrl, campaignDTO);
				Date endDate = new Date();
				long difference = endDate.getTime() - startDate.getTime();
				log.info("Plivo - Initiated call for campaign [{}] and pcid [{}], time difference [{}]", campaignDTO.getId(),
						prospectCallDTO.getProspectCallId(), difference);
			} else if (callProvider.equalsIgnoreCase("Telnyx")) {
				log.info("Telnyx - Initiating call for campaign [{}] and pcid [{}]", campaignDTO.getId(),
						prospectCallDTO.getProspectCallId());
				Date startDate = new Date();
				callSid = telnyxCallService.placeCall(prospectCallDTO, outboundNumber, answerUrl, campaignDTO);
				Date endDate = new Date();
				long difference = endDate.getTime() - startDate.getTime();
				log.info("Telnyx - Initiated call for campaign [{}] and pcid [{}], time difference [{}]", campaignDTO.getId(),
						prospectCallDTO.getProspectCallId(), difference);
			} else if (callProvider.equalsIgnoreCase("Signalwire")) {
				log.info("Signalwire - Initiating call for campaign [{}] and pcid [{}]", campaignDTO.getId(),
						prospectCallDTO.getProspectCallId());
				Date startDate = new Date();
				callSid = signalwireCallService.placeCall(prospectCallDTO, outboundNumber, answerUrl, campaignDTO);
				Date endDate = new Date();
				long difference = endDate.getTime() - startDate.getTime();
				log.info("Signalwire - Initiated call for campaign [{}] and pcid [{}], time difference [{}]", campaignDTO.getId(),
						prospectCallDTO.getProspectCallId(), difference);
			} else {
				log.info("Twilio - Initiating call for campaign [{}] and pcid [{}]", campaignDTO.getId(),
						prospectCallDTO.getProspectCallId());
				Date startDate = new Date();
				callSid = twilioCallService.placeCall(prospectCallDTO, outboundNumber, answerUrl, campaignDTO);
				Date endDate = new Date();
				long difference = endDate.getTime() - startDate.getTime();
				log.info("Twilio - Initiated call for campaign [{}] and pcid [{}], time difference [{}]", campaignDTO.getId(),
						prospectCallDTO.getProspectCallId(), difference);
			}
			/*
			 * add callSid in map only for power call so that we can subtract this value
			 * while fetching new prospects for dialing
			 */
			if (callSid != null && callProvider.equalsIgnoreCase("Plivo")) {
				plivoCallStatusMap.put(callSid, "queued");
			} else if (callSid != null && callProvider.equalsIgnoreCase("Telnyx")) {
				log.info("Adding CallSid [{}] to telnyxCallLegIdMap", callSid);
				telnyxCallLegIdMap.put(callSid, "queued");
			} else if (callSid != null && campaignDTO.getCallControlProvider().equalsIgnoreCase("Signalwire")) {
				log.info("Adding CallSid [{}] to signalwireCallStatusMap", callSid);
				signalwireCallStatusMap.put(callSid, "queued");
			} else {
				callStatusMap.put(callSid, "queued");
			}
		} else if (campaignDTO.getDialerMode().equals(DialerMode.RESEARCH_AND_DIAL)
				&& (prospect.getProspectType().equalsIgnoreCase(Constants.PROSPECT_TYPE.PREVIEW.name()))
				|| prospect.getProspectType().equalsIgnoreCase(Constants.PROSPECT_TYPE.RESEARCH.name())) {
			callSid = randomUUID;
		} else {
			callSid = randomUUID;
		}

		if (callSid != null) {
			int callRetryCount = prospectCallDTO.getCallRetryCount() + 1;
			Long dailyCallRetryCount = incrementDailyRetryCount(prospectCallDTO.getProspect().getTimeZone(),
					prospectCallDTO.getDailyCallRetryCount());
			String prospectInteractionSessionId = "IS" + StringUtils.replace(UUID.randomUUID().toString(), "-", "");
			log.info(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("Auto call generated on dialer")
					.processName("AutoCall").methodName("processProspectCall")
					.prospectCallId(prospectCallDTO.getProspectCallId())
					.prospectInteractionSessionId(prospectInteractionSessionId).campaignId(campaignDTO.getId())
					.callSid(callSid).outboundNumber(outboundNumber).callRetryCount(Integer.toString(callRetryCount))
					.dailyCallRetryCount(Long.toString(dailyCallRetryCount))
					.dialerMode(campaignDTO.getDialerMode().name())
					.voiceProvider(campaignDTO.getCallControlProvider()).build());

			prospectCallDTO.setTwilioCallSid(callSid);
			prospectCallDTO.setOutboundNumber(outboundNumber);
			prospectCallDTO.setProspectInteractionSessionId(prospectInteractionSessionId);
			prospectCallDTO.setCallRetryCount(callRetryCount);
			prospectCallDTO.setDailyCallRetryCount(dailyCallRetryCount);
			// callStatusMap.put(callSid, "queued"); // handled above for power call
			return ProspectCallStatus.CALL_DIALED;
		} else {
			log.info(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("AutoCall - CALL_FAILED")
					.processName("AutoCall").methodName("processProspectCall")
					.prospectCallId(prospectCallDTO.getProspectCallId()).callSid(callSid).outboundNumber(outboundNumber)
					.build());
			return ProspectCallStatus.CALL_FAILED;
		}
	}

	@Async
	public void cancelPendingCalls(List<String> callSidList) {
		callSidList = getPendingCalls(callSidList);
		callSidList.forEach(callSid -> {
			String status = twilioCallService.getCallStatus(callSid);
			log.info("Found the status: {} for call side: {}", status, callSid);
			if (!status.equals("answered") && !status.equals("completed") && !status.equals("failed")
					&& !status.equals("no-answer") && !status.equals("in-progress")) {
				twilioCallService.cancelCall(callSid);
			}
		});
	}

	@Async
	public void cancelPendingCalls(List<String> callSidList, int count) {
		callSidList = getPendingCalls(callSidList);
		for (int i = callSidList.size() - 1; i >= 0; i--) {
			if (count > 0) {
				String status = twilioCallService.getCallStatus(callSidList.get(i));
				log.info("Found the status: {} for call side: {}", status, callSidList.get(i));
				if (!status.equals("answered") && !status.equals("completed") && !status.equals("failed")
						&& !status.equals("no-answer") && !status.equals("in-progress")) {
					count--;
					twilioCallService.cancelCall(callSidList.get(i));
				}
			}
		}
	}

	/**
	 * Twilio status callback - update prospectcalllog & prospectcallinteraction
	 *
	 * @param callSid
	 * @param callStatus
	 */
	public void processCallStatus(String callSid, String callStatus, String answeredBy, String machineAnsweredDetection) {
		if (callStatusMap.containsKey(callSid)) {
			if (!callStatus.equals(callStatusMap.get(callSid))) {
				ProspectCallStatus prospectCallStatus = getProspectCallStatus(callStatus);
				switch (callStatus) {
					case "answered":
					case "completed":
					case "failed":
					case "no-answer":
					case "in-progress":
					case "canceled":
					case "busy":
					case "machine":
						callStatusMap.remove(callSid);
						if (!callStatus.equals("in-progress")) {
							prospectService.updateProspect(null, null, callSid, prospectCallStatus, 0, null, null);
						}
						if (answeredBy != null && answeredBy.equalsIgnoreCase("machine_start") && machineAnsweredDetection != null
								&& machineAnsweredDetection.equalsIgnoreCase("true")) {
							twilioCallService.cancelCall(callSid);
							prospectService.updateProspect(null, null, callSid, ProspectCallStatus.CALL_MACHINE_ANSWERED, 0, null,
									null);
						}
						log.info("Size of twilioCallStatusMap [{}] : ", callStatusMap.size());
				}
			}
		}
	}

	public List<String> getPendingCalls(List<String> callSidList) {
		return callSidList.stream().filter(callSid -> callStatusMap.containsKey(callSid)).collect(Collectors.toList());
	}

	public String registerCallDetails(String callSid, ProspectCallDTO prospectCallDTO) {
		String registerCallUrl = String.format(REGISTER_CALL_URL_TEMPLATE, callSid);
		return xtaasService.post(registerCallUrl, new ParameterizedTypeReference<String>() {
		}, prospectCallDTO);
	}

	public TelnyxCallStatusDTO fetchCallDetails(String callSid, ProspectCallDTO prospectCallDTO) {
		String fetchCallUrl = String.format(REGISTER_CALL_URL_TEMPLATE, callSid);
		return xtaasService.get(fetchCallUrl, new ParameterizedTypeReference<TelnyxCallStatusDTO>() {});
	}

	private long incrementDailyRetryCount(String timeZone, Long dailyRetryCount) {
		if (timeZone == null || timeZone.isEmpty()) {
			timeZone = "US/Pacific";
		}
		Calendar time = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
		long baseToday = time.get(Calendar.YEAR) * 10000 + (time.get(Calendar.MONTH) + 1) * 100 + time.get(Calendar.DATE);

		if (dailyRetryCount == null || dailyRetryCount / 100 != baseToday) {
			return baseToday * 100 + 1;
		} else {
			return (baseToday * 100) + (dailyRetryCount % 100) + 1;
		}
	}

	/**
	 * Compare twilio status with XTaaS status
	 *
	 * @param twilioCallStatus
	 * @return ProspectCallStatus
	 */
	private ProspectCallStatus getProspectCallStatus(String twilioCallStatus) {
		if (TwilioStatusConstants.TWILIO_CALL_STATUS_NO_ANSWER.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_NO_ANSWER;
		} else if (TwilioStatusConstants.TWILIO_CALL_STATUS_BUSY.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_BUSY;
		} else if (TwilioStatusConstants.TWILIO_CALL_STATUS_CANCELED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_CANCELED;
		} else if (TwilioStatusConstants.TWILIO_CALL_STATUS_FAILED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_FAILED;
		} else if (TwilioStatusConstants.TWILIO_CALL_MACHINE_ANSWERED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_MACHINE_ANSWERED;
		} else {
			return null;
		}
	}

	/**
	 * Twilio status callback - update prospectcalllog & prospectcallinteraction
	 *
	 * @param callSid
	 * @param callStatus
	 */
	public void processPlivoCallStatus(HttpServletRequest request, HttpServletResponse response) {
		HashMap<String, String> paramMap = new HashMap<String, String>();
		for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
			String param = params.nextElement();
			paramMap.put(param, request.getParameter(param));
		}
		String callSid = paramMap.get("CallUUID");
		String callStatus = paramMap.get("CallStatus");
		String hangupSource = paramMap.get("HangupSource");
		String hangupCauseName = paramMap.get("HangupCauseName");
		String answeredBy = paramMap.get("Machine");
		String machineAnsweredDetection = paramMap.get("MachineAnsweredDetection");
		// Commented code for call should disconnect by prospect issue.
		if (callStatus.equalsIgnoreCase("completed") && hangupSource.equalsIgnoreCase("Callee")) {
			prospectService.notifyCallDisconnectToAgent(callSid);
		}
		if (plivoCallStatusMap.containsKey(callSid)) {
			if (!callStatus.equals(plivoCallStatusMap.get(callSid))) {
				ProspectCallStatus prospectCallStatus = getPlivoProspectCallStatus(callStatus, hangupSource,
						hangupCauseName);
				log.info("Plivo Call Status Update - CallSid [{}], HangupSource [{}], HangupCause [{}] <=====", callSid,
						hangupSource, hangupCauseName);
				switch (callStatus) {
				case "answered":
				case "completed":
				case "failed":
				case "no-answer":
				case "in-progress":
				case "canceled":
				case "busy":
				case "machine":
				case "timeout":
					// Handle timeout callstatus also
					plivoCallStatusMap.remove(callSid);
					if (prospectCallStatus != null && !callStatus.equals("in-progress")) {
						prospectService.updateProspect(null, null, callSid, prospectCallStatus, 0, null, null);
					}
					/* Plivo Synchronous Answering Machine Detection */
					if (answeredBy != null && answeredBy.equalsIgnoreCase("true") && machineAnsweredDetection != null
							&& machineAnsweredDetection.equalsIgnoreCase("true")) {
						prospectService.updateProspect(null, null, callSid, ProspectCallStatus.CALL_MACHINE_ANSWERED, 0,
								null, null);
						xtaasService.post(XtaasConstants.PLIVO_SYNC_AMD_URL, new ParameterizedTypeReference<String>() {
						}, paramMap);
					}
					log.info("Size of plivoCallStatusMap [{}] : ", plivoCallStatusMap.size());
				}
			} else {
				// write else block to update the prospect to fix prospects Stuck in CALL_DIALED status.
				log.info(
						"processPlivoCallStatus() :- The CallSid [{}] status is same in webhook and plivocallstausmap. CallStatus [{}], Map status [{}] <=====",
						callSid, callStatus, plivoCallStatusMap.get(callSid));
			}
		} else {
			// write else block to update the prospect to fix prospects Stuck in CALL_DIALED status.
			log.info(
					"processPlivoCallStatus() :- The CallSid [{}] does not found in plivoCallStatusMap. CallStatus [{}], HangupCause [{}] <=====",
					callSid, callStatus, hangupCauseName);
			ProspectCallStatus prospectCallStatus = getPlivoProspectCallStatus(callStatus, hangupSource,
						hangupCauseName);
			if (prospectCallStatus != null && !callStatus.equals("in-progress")) {
				prospectService.updateProspect(null, null, callSid, prospectCallStatus, 0, null, null);
			}
		}
	}


	public void removeAnsweredCallSid(String callSid) {
		log.info("Removing CallSid [{}] from plivoCallStatusMap", callSid);
		plivoCallStatusMap.remove(callSid);
		log.info("Removed CallSid [{}] from plivoCallStatusMap [{}] : ", callSid, plivoCallStatusMap.size());
	}

	public void removeTelnyxCallSid(String callSid) {
		log.info("Removing CallSid [{}] from telnyxCallLegIdMap", callSid);
		telnyxCallLegIdMap.remove(callSid);
		log.info("Removed CallSid [{}] from telnyxCallLegIdMap [{}] : ", callSid, telnyxCallLegIdMap.size());
	}

	public void removeSignalwireAnsweredCallSid(String callSid) {
		log.info("Removing CallSid [{}] from signalwireCallStatusMap", callSid);
		signalwireCallStatusMap.remove(callSid);
		log.info("Removed CallSid [{}] from signalwireCallStatusMap [{}] : ", callSid, signalwireCallStatusMap.size());
	}

	public List<String> getPlivoPendingCalls(List<String> callSidList) {
		return callSidList.stream().filter(callSid -> plivoCallStatusMap.containsKey(callSid)).collect(Collectors.toList());
	}

	public List<String> getTelnyxPendingCalls(List<String> callSidList) {
		return callSidList.stream().filter(callSid -> telnyxCallLegIdMap.containsKey(callSid)).collect(Collectors.toList());
	}

	public List<String> getSignalwirePendingCalls(List<String> callSidList) {
		return callSidList.stream().filter(callSid -> signalwireCallStatusMap.containsKey(callSid)).collect(Collectors.toList());
	}

	public void emptySignalwireCallStatusMap() {
		signalwireCallStatusMap.clear();
	}

	@Async
	public void cancelPlivoPendingCalls(List<String> callSidList) {
		callSidList = getPlivoPendingCalls(callSidList);
		callSidList.forEach(callSid -> {
			String status = plivoCallService.getPlivoCallStatus(callSid);
			log.info("Plivo Call Cancel - CallSid [{}], CallStatus [{}]", callSid, status);
			if (!status.equalsIgnoreCase("IN_PROGRESS")) {
				try {
					removeAnsweredCallSid(callSid);
					plivoCallService.cancelPlivoCall(callSid);
				} catch (Exception e) {
					log.error("cancelPlivoPendingCalls - Error occurred while cancelling call. CallSid [{}]", callSid);
					log.error("Exception is: [{}]", e.getMessage());
				}
			}
		});
	}

	@Async
	public void cancelTelnyxPendingCalls(List<String> callSidList) {
		callSidList = getTelnyxPendingCalls(callSidList);
		callSidList.forEach(callSid -> {
			boolean isAlive = telnyxCallService.getTelnyxCallStatus(callSid);
			log.info("Telnyx Call Cancel - CallSid [{}], IsAlive [{}]", callSid, isAlive);
			if (!isAlive) {
				try {
					telnyxCallService.cancelTelnyxCall(callSid);
				} catch (Exception e) {
					log.error("cancelTelnyxPendingCalls - Error occurred while cancelling call. CallSid [{}]", callSid);
					log.error("Exception is: [{}]", e.getMessage());
				}
			}
		});
	}

	@Async
	public void cancelPlivoPendingCalls(List<String> callSidList, int count) {
		callSidList = getPlivoPendingCalls(callSidList);
		for (int i = callSidList.size() - 1; i >= 0; i--) {
			if (count > 0) {
				String callSid = callSidList.get(i);
				String status = plivoCallService.getPlivoCallStatus(callSid);
				log.info("Plivo Call Cancel - CallSid [{}], CallStatus [{}]", callSid, status);
				if (!status.equalsIgnoreCase("IN_PROGRESS")) {
					count--;
					try {
						plivoCallService.cancelPlivoCall(callSid);
					} catch (Exception e) {
						log.error("cancelPlivoPendingCalls - Error occurred while cancelling call. CallSid [{}]", callSid);
						log.error("Exception is: [{}]", e.getMessage());
					}
				}
			}
		}
	}

	@Async
	public void cancelTelnyxPendingCalls(List<String> callSidList, int count) {
		callSidList = getTelnyxPendingCalls(callSidList);
		for (int i = callSidList.size() - 1; i >= 0; i--) {
			if (count > 0) {
				String callSid = callSidList.get(i);
				boolean isAlive = telnyxCallService.getTelnyxCallStatus(callSid);
				log.info("Telnyx Call Cancel - CallSid [{}], IsAlive [{}]", callSid, isAlive);
				if (!isAlive) {
					count--;
					try {
						telnyxCallService.cancelTelnyxCall(callSid);
					} catch (Exception e) {
						log.error("cancelTelnyxPendingCalls - Error occurred while cancelling call. CallSid [{}]", callSid);
						log.error("Exception is: [{}]", e.getMessage());
					}
				}
			}
		}
	}

	@Async
	public void cancelSignalwirePendingCalls(List<String> callSidList) {
		callSidList = getSignalwirePendingCalls(callSidList);
		callSidList.forEach(callSid -> {
			// String status = plivoCallService.getPlivoCallStatus(callSid);
			// log.info("Signalwire Call Cancel - CallSid [{}], CallStatus [{}]", callSid, status);
			log.info("Signalwire Call Cancel - CallSid [{}]", callSid);
			// if (!status.equalsIgnoreCase("IN_PROGRESS")) {
			try {
				removeSignalwireAnsweredCallSid(callSid);
				// plivoCallService.cancelPlivoCall(callSid);
			} catch (Exception e) {
				log.error("cancelSignalwirePendingCalls - Error occurred while cancelling call. CallSid [{}]", callSid);
				log.error("Exception is: [{}]", e.getMessage());
			}
			// }
		});
	}

	/**
	 * Compare twilio status with XTaaS status
	 *
	 * @param twilioCallStatus
	 * @return ProspectCallStatus
	 */
	private ProspectCallStatus getPlivoProspectCallStatus(String plivoCallStatus, String hangupSource,
			String hangupCauseName) {
		if (PlivoStatusConstants.PLIVO_CALL_STATUS_COMPLETED.equals(plivoCallStatus) && hangupCauseName != null) {
			if (hangupCauseName.equalsIgnoreCase(PlivoHangupCause.CANCELLED.toString())) {
				return ProspectCallStatus.CALL_CANCELED;
			} else if (hangupCauseName.equalsIgnoreCase(PlivoHangupCause.RING_TIMEOUT_REACHED.toString())) {
				return ProspectCallStatus.CALL_NO_ANSWER;
			} else {
				return null;
			}
		} else if (PlivoStatusConstants.PLIVO_CALL_STATUS_NO_ANSWER.equals(plivoCallStatus)) {
			return ProspectCallStatus.CALL_NO_ANSWER;
		} else if (PlivoStatusConstants.PLIVO_CALL_STATUS_BUSY.equals(plivoCallStatus)) {
			return ProspectCallStatus.CALL_BUSY;
		} else if (PlivoStatusConstants.PLIVO_CALL_STATUS_CANCELED.equals(plivoCallStatus)) {
			return ProspectCallStatus.CALL_CANCELED;
		} else if (PlivoStatusConstants.PLIVO_CALL_STATUS_FAILED.equals(plivoCallStatus)) {
			return ProspectCallStatus.CALL_FAILED;
		} else if (PlivoStatusConstants.PLIVO_CALL_MACHINE_ANSWERED.equals(plivoCallStatus)) {
			return ProspectCallStatus.CALL_MACHINE_ANSWERED;
		} else {
			return null;
		}
	}

}
