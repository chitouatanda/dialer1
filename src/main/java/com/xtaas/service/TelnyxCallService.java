package com.xtaas.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xtaas.domain.Prospect;
import com.xtaas.service.dto.CallInfoDTO;
import com.xtaas.service.dto.CampaignDTO;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.dto.TelnyxCallDTO;
import com.xtaas.service.dto.TelnyxSIPInviteObject;
import com.xtaas.service.util.XtaasConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class TelnyxCallService {

  private final Logger log = LoggerFactory.getLogger(TelnyxCallService.class);
  private final String thinQDomainUrl;
  private final String telnyxDomainUrl;
  private final String telnyxToken;
  private final CacheService cacheService;
  private final String telnyxBaseUrl;
  private final String telnyxConnectionId;
  private static final String FETCH_CALLCONTROL_URL_TEMPLATE = "/spr/telnyx/call/control/%s";
  private final String thinQId;
  private final String thinQToken;
  private final String domainViva;
	private final String accountIdViva;
	private final String passcodeViva;
  private final String serverBaseUrl;
  private final String usernameSamespace;
  private final String passwordSamespace;
  private final String domainSamespace;

  @Autowired
  HttpService httpService;

  @Autowired
  XTaaSService xTaaSService;

  public TelnyxCallService(CacheService cacheService) {
    this.cacheService = cacheService;
    this.thinQDomainUrl = System.getenv(XtaasConstants.THINQ_DOMAIN_URL);
    this.telnyxDomainUrl = System.getenv(XtaasConstants.TELNYX_DOMAIN_URL);
    this.telnyxToken = System.getenv(XtaasConstants.TELNYX_TOKEN);
    this.telnyxBaseUrl = System.getenv(XtaasConstants.TELNYX_BASE_APIURL);
    this.telnyxConnectionId = System.getenv(XtaasConstants.TELNYX_CONNECTION_ID);
    this.thinQId = System.getenv(XtaasConstants.THINQ_ID);
    this.thinQToken = System.getenv(XtaasConstants.THINQ_TOKEN);
    this.domainViva = System.getenv(XtaasConstants.VIVA_TELNYX_DOMAIN);
		this.accountIdViva = System.getenv(XtaasConstants.VIVA_TELNYX_ACCOUNTID);
		this.passcodeViva = System.getenv(XtaasConstants.VIVA_TELNYX_PASSCODE);
    this.serverBaseUrl = System.getenv(XtaasConstants.DIALER_XTAAS_BASE_URL);
    this.domainSamespace = System.getenv(XtaasConstants.SAMESPACE_DOMAIN);
    this.usernameSamespace = System.getenv(XtaasConstants.SAMESPACE_USERNAME);
    this.passwordSamespace = System.getenv(XtaasConstants.SAMESPACE_PASSWORD);
  }

  private HttpHeaders headers() {
    HttpHeaders headers = new HttpHeaders();

    headers.add("accept", "application/json");
    headers.add("content-type", "application/json");
    // headers.add("Authorization", "Bearer
    // KEY01714F0AC7E9C540B56E47E500976FBD_oBcpTNGLUXuBaWb11GF0t0");
    // headers.add("Authorization", "Bearer KEY01714F0AC7E9C540B56E47E500976FBD_oBcpTNGLUXuBaWb11GF0t0");
    headers.add("Authorization", "Bearer KEY0172084AA30002F82C7A7F8E2BCE2D7E_BgM5PHypNaVMLkSmhD4UrE");
    return headers;
  }

  private URI buildUri(String resourcePath) {
    return UriComponentsBuilder.fromUriString(telnyxBaseUrl + resourcePath).build(true).toUri();
  }

  private CallInfoDTO decodeCallInfo(String clientState) {
    try {
      CallInfoDTO agentCallInfo = new ObjectMapper().readValue(new String(Base64.getDecoder().decode(clientState)),
          CallInfoDTO.class);
      return agentCallInfo;
    } catch (JsonParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (JsonMappingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  private String encodeCallInfo(CallInfoDTO callInfo) {
    try {
      String encodedString = Base64.getEncoder()
          .encodeToString(new ObjectMapper().writeValueAsString(callInfo).getBytes());
      return encodedString;
    } catch (JsonProcessingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return "";
    }
  }

  public String placeCall(ProspectCallDTO prospectCallDTO, String fromNumber, String answerUrl, CampaignDTO campaignDTO)
      throws URISyntaxException {
    String callLegId = null;
    String pcid = prospectCallDTO.getProspectCallId();
    String toNumber = prospectCallDTO.getProspect().getPhone();
    /*
     * AMD on dataSlice, callRetryCount > 0 & enableMachineAnsweredDetection flags
     */
    String dataSlice = prospectCallDTO.getDataSlice();
    int callRetryCount = prospectCallDTO.getCallRetryCount();
    boolean useSlice = campaignDTO.isUseSlice();
    boolean disableAMDOnQueued = campaignDTO.isDisableAMDOnQueued();
    boolean enableMachineAnsweredDetection = campaignDTO.isEnableMachineAnsweredDetection();
    // Long callTimeLimit = cacheService.getLongApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.PLIVO_MAX_CALL_TIME_LIMIT.name(), 1800);
    
    TelnyxCallDTO request = new TelnyxCallDTO();
    if (campaignDTO.getTelnyxCallTimeLimitSeconds() > 0 && campaignDTO.getTelnyxCallTimeLimitSeconds() < 60) {
			request.setTime_limit_secs(60);
		} else if (campaignDTO.getTelnyxCallTimeLimitSeconds() >= 60 && campaignDTO.getTelnyxCallTimeLimitSeconds() <= 14400) {
			request.setTime_limit_secs(campaignDTO.getTelnyxCallTimeLimitSeconds());
		} else if (campaignDTO.getTelnyxCallTimeLimitSeconds() > 14400) {
			request.setTime_limit_secs(14400);
		} else {
			request.setTime_limit_secs(1800);
		}
    if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("ThinQ")) {
      
    } else if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("Viva")) {
      request.setSip_auth_username(accountIdViva);
      request.setSip_auth_password(passcodeViva);
      toNumber = createVivaNumber(toNumber, prospectCallDTO.getProspect().getCountry());
    } else if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("Samespace")) {
      request.setSip_auth_username(usernameSamespace);
      request.setSip_auth_password(passwordSamespace);
      toNumber = createSamespaceNumber(toNumber, prospectCallDTO.getProspect().getCountry());
    } else {
      String countryName = prospectCallDTO.getProspect().getCountry();
      if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
          || countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
          || countryName.equalsIgnoreCase("CA"))) {
        toNumber = getSimpleFormattedUSPhoneNumber(toNumber);
      }
    }
    Prospect prospect = prospectCallDTO.getProspect();
    CallInfoDTO callInfo = new CallInfoDTO();
    callInfo.setCampaignId(campaignDTO.getId());
    callInfo.setHangup(true);
    callInfo.setPcid(pcid);
    callInfo.setMachineAnsweredDetection(campaignDTO.isEnableMachineAnsweredDetection());
    callInfo.setDialer("1");
    callInfo.setWhisperMsg(prospect.getFirstName() + " " + prospect.getLastName());
    callInfo.setCallerId(fromNumber);
    callInfo.setAutoMachineHangup(campaignDTO.isAutoMachineHangup());
    String clientStateData = encodeCallInfo(callInfo);

    request.setConnection_id(telnyxConnectionId);
    request.setFrom(fromNumber);
    request.setTo(toNumber);
    request.setClient_state(clientStateData);
    URI uri = buildUri("/calls");

    Map<String, Object> dialCallResponse = null;
    if (enableMachineAnsweredDetection && useSlice && disableAMDOnQueued && callRetryCount > 0 && dataSlice != null
        && !dataSlice.equalsIgnoreCase("slice0") && !dataSlice.equalsIgnoreCase("slice10")) {
      /*
       * AMD on data which is not in slice0, slice10 & Queued --
       * enableMachineAnsweredDetection is true, useSlice is true, disableAMDOnQueued
       * is true, callRetryCount > 0 (i.e. 2nd retry) & dataSlice is slice1
       */

      request.setAnswering_machine_detection("detect");

    } else if (enableMachineAnsweredDetection && useSlice && !disableAMDOnQueued && dataSlice != null
        && !dataSlice.equalsIgnoreCase("slice0") && !dataSlice.equalsIgnoreCase("slice10")) {
      /*
       * AMD on data which is not in slice0 & slice10 --
       * enableMachineAnsweredDetection is true, useSlice is true, disableAMDOnQueued
       * is false & dataSlice is slice1
       */

      request.setAnswering_machine_detection("detect");

    } else if (enableMachineAnsweredDetection && !useSlice) {
      /*
       * AMD on all calls -- enableMachineAnsweredDetection is true & useSlice is
       * false
       */

      request.setAnswering_machine_detection("detect");

    } else {
      // NO AMD
      request.setAnswering_machine_detection("disabled");

    }
    try {
      dialCallResponse = (Map<String, Object>) httpService
          .post(uri, headers(), new ParameterizedTypeReference<Map<String, Object>>() {
          }, new ObjectMapper().writeValueAsString(request)).get("data");
    } catch (JsonProcessingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    callLegId = (String) dialCallResponse.get("call_leg_id");
    log.info("Dialer TelnyxCallService========>Call Dialed from Dialer , Call LegId : " + callLegId + ", pcid : [{}]" + pcid + ", CampaignId : " + campaignDTO.getId());
    // log.info("Call leg Id: " + callLegId);
    return callLegId;
  }

  private String createThinQNumber(String phoneNumber, String countryName) {
    String toNumber = "";
    if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
        || countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
        || countryName.equalsIgnoreCase("CA"))) {
      toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
    } else {
      toNumber = phoneNumber;
    }
    StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@").append(thinQDomainUrl);
    return thinQNumber.toString();
  }

  private static String getSimpleFormattedUSPhoneNumber(String phoneNumber) {
    if (phoneNumber == null) {
      return null;
    }
    String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d.]", ""); // removes all non-numeric chars
    String formattedPhoneNumber = normalizedPhoneNumber;
    if (normalizedPhoneNumber.length() == 10) {
      formattedPhoneNumber = "+1" + normalizedPhoneNumber;
    }
    return formattedPhoneNumber;
  }

  private String createTelnyxNumber(String phoneNumber, String countryName) {
    String toNumber = "";
    if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
        || countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
        || countryName.equalsIgnoreCase("CA"))) {
      toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
    } else {
      toNumber = phoneNumber;
    }
    StringBuilder telnyxNumber = new StringBuilder("sip:").append(toNumber).append("@").append(telnyxDomainUrl);
    return telnyxNumber.toString();
  }
  
  private String createVivaNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedVivaUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
//			toNumber = "+" + toNumber;
		}
		 StringBuilder vivaNumber = new StringBuilder("sip:").append(toNumber).append("@").append(domainViva);
		return vivaNumber.toString();
  }
  
  private String createSamespaceNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedSamespaceUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			// toNumber = "+" + toNumber;
		}
		StringBuilder samespaceNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(domainSamespace);
		return samespaceNumber.toString();
	}

	private String getSimpleFormattedVivaUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		// removes all non-numeric chars
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = "1" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
  }
  
  private String getSimpleFormattedSamespaceUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		// removes all non-numeric chars
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = "1" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

  public boolean getTelnyxCallStatus(String callLegId) {
    boolean isAlive = false;
    String registerCallUrl = String.format(FETCH_CALLCONTROL_URL_TEMPLATE, callLegId);
    String callControlId = xTaaSService.get(registerCallUrl, new ParameterizedTypeReference<String>(){});
    URI uri = buildUri("/calls/" + callControlId);
    Map<String, Object> response = httpService.get(uri, headers(),
        new ParameterizedTypeReference<Map<String, Object>>() {
        });
    if (response != null && response.containsKey("data") && response.get("data") instanceof HashMap) {
      HashMap<String, Object> infoMap = (HashMap<String, Object>) response.get("data");
      isAlive = (boolean) infoMap.get("is_alive");
    }
    return isAlive;
  }

  public void cancelTelnyxCall(String callLegId) {
    String registerCallUrl = String.format(FETCH_CALLCONTROL_URL_TEMPLATE, callLegId);
    String callControlId = xTaaSService.get(registerCallUrl, new ParameterizedTypeReference<String>(){});
    URI uri = buildUri("/calls/" + callControlId + "/actions/hangup");
    Map<String, Object> response = httpService.post(uri, headers(),
        new ParameterizedTypeReference<Map<String, Object>>() {
        }, "");
    log.info("Call {} deleted successfully.", callLegId);
  }

}