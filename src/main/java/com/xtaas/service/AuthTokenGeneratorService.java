package com.xtaas.service;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Service;

import com.xtaas.service.util.AppManager;
import com.xtaas.service.util.AppManager.App;
import com.xtaas.service.util.EncryptionUtils;

@Service
public class AuthTokenGeneratorService {

	public String generateSystemToken(String appName, String systemUser) {
		// generate authdata in format username|org|role|time in MS
		return generateToken(appName, systemUser, "XTAAS", "ROLE_SYSTEM");
	}

	public String generateToken(String appName, String userName, String organization, String roles) {
		// generate authdata in format username|org|role|time in MS
		StringBuilder authDataBuilder = new StringBuilder();
		authDataBuilder.append(userName).append("|"); // user
		authDataBuilder.append(organization).append("|"); // org
		authDataBuilder.append(roles).append("|"); // roles (comma separated list)
		authDataBuilder.append(System.currentTimeMillis()); // time in MS when token was generated
		String authData = authDataBuilder.toString();

		// generate token in the format appname:data:hash
		String appSecurityKey = AppManager.getSecurityKey(EnumUtils.getEnum(App.class, appName));
		return appName + ":" + EncryptionUtils.encrypt(authData, appSecurityKey) + ":"
				+ EncryptionUtils.generateHash(authData, appSecurityKey);
	}

}
