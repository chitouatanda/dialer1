package com.xtaas.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xtaas.domain.OutboundNumber;
import com.xtaas.service.dto.CampaignDTO;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.dto.SignalwireCreateCallDTO;
import com.xtaas.service.util.XtaasConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class SignalwireCallService {

    private final Logger log = LoggerFactory.getLogger(SignalwireCallService.class);
    private final String signalwireProjectId;
    private final String signalwireApplicationSid;
    private final String signalwireAuthToken;
    private final String signalwireBaseUrl;
    private final String xtaasBaseUrl;
    private final String signalwireTelnyxToken;
    private final String telnyxDomainUrl;
    private final CacheService cacheService;
    private final String plivoSIPDomain;
    private final String plivoSIPUsername;
    private final String plivoSIPPassword;
    private final String thinQDomainUrl;
    private final String thinQId;
	private final String thinQToken;

    @Autowired
    HttpService httpService;

    public SignalwireCallService(CacheService cacheService) {
        this.signalwireApplicationSid = System.getenv(XtaasConstants.SIGNALWIRE_APPLICATIONSID);
        this.signalwireProjectId = System.getenv(XtaasConstants.SIGNALWIRE_PROJECTID);
        this.signalwireBaseUrl = System.getenv(XtaasConstants.SIGNALWIRE_BASE_URL);
        this.signalwireAuthToken = System.getenv(XtaasConstants.SIGNALWIRE_AUTH_TOKEN);
        this.xtaasBaseUrl = System.getenv(XtaasConstants.DIALER_XTAAS_BASE_URL);
        this.signalwireTelnyxToken = System.getenv(XtaasConstants.SIGNALWIRE_TELNYX_TOKEN);
        this.telnyxDomainUrl = System.getenv(XtaasConstants.TELNYX_DOMAIN_URL);
        this.cacheService = cacheService;
        this.plivoSIPDomain = System.getenv(XtaasConstants.PLIVO_SIP_DOMAIN);
        this.plivoSIPUsername = System.getenv(XtaasConstants.PLIVO_SIP_USERNAME);
        this.plivoSIPPassword = System.getenv(XtaasConstants.PLIVO_SIP_PASSWORD);
        this.thinQDomainUrl = System.getenv(XtaasConstants.THINQ_DOMAIN_URL);
        this.thinQId = System.getenv(XtaasConstants.THINQ_ID);
		this.thinQToken = System.getenv(XtaasConstants.SIGNALWIRE_THINQ_TOKEN);
    }

    private URI buildUri(String resourcePath) {
        return UriComponentsBuilder.fromUriString(signalwireBaseUrl + "/" + signalwireProjectId + resourcePath)
                .build(true).toUri();
    }

    private HttpHeaders headers() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "*/*");
        headers.add("content-type", "application/x-www-form-urlencoded");
        // headers.add("Authorization",
        // "Basic
        // YzQ5YTUxMWEtMmQ3YS00ODE2LWIxMzktMzAyYWU3ZDdjNzRkOlBUMTc2ZGE5Y2ZiMGRlYTk0ZGRlYTU0Njg1OWYyZjJlYjVlY2U4ZjYwN2Q3OTNiMThm");
        headers.add("Authorization", "Basic " + getBase64EncodedAuthorizationString());
        return headers;
    }

    private String getBase64EncodedAuthorizationString() {
        String encodedString = Base64.getEncoder()
                .encodeToString((signalwireProjectId + ":" + signalwireAuthToken).getBytes());
        return encodedString;
    }

    public String placeCall(ProspectCallDTO prospectCallDTO, String fromNumber, String answerUrl,
            CampaignDTO campaignDTO) throws URISyntaxException {
        String callSid = null;
        String pcid = prospectCallDTO.getProspectCallId();
        String toNumber = prospectCallDTO.getProspect().getPhone();
        /*
         * AMD on dataSlice, callRetryCount > 0 & enableMachineAnsweredDetection flags
         */
        String dataSlice = prospectCallDTO.getDataSlice();
        int callRetryCount = prospectCallDTO.getCallRetryCount();
        // boolean directPhone = prospectCallDTO.getProspect().isDirectPhone();
        boolean useSlice = campaignDTO.isUseSlice();
        boolean disableAMDOnQueued = campaignDTO.isDisableAMDOnQueued();
        boolean enableMachineAnsweredDetection = campaignDTO.isEnableMachineAnsweredDetection();
        // try {
        // Long callTimeLimit = cacheService.getLongApplicationPropertyValue(
        // XtaasConstants.APPLICATION_PROPERTY.PLIVO_MAX_CALL_TIME_LIMIT.name(), 1800);
        // Long maxCallRingingDuration = cacheService.getLongApplicationPropertyValue(
        // XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION_AUTO_DIAL.name(),
        // 60);

        String countryName = prospectCallDTO.getProspect().getCountry();
        MultiValueMap<String, String> map = new LinkedMultiValueMap();
        if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("Plivo")) {
            toNumber = createPlivoSIPNumber(toNumber, prospectCallDTO.getProspect().getCountry());
            map.add("SipAuthUsername", plivoSIPUsername);
            map.add("SipAuthPassword", plivoSIPPassword);
        } else if (campaignDTO.getSipProvider() != null && campaignDTO.getSipProvider().equalsIgnoreCase("Telnyx")) {
            toNumber = createTelnyxNumber(toNumber, prospectCallDTO.getProspect().getCountry());
        // } else if (countryName != null && (countryName.equalsIgnoreCase("United States")
        //         || countryName.equalsIgnoreCase("USA") || countryName.equalsIgnoreCase("US")
        //         || countryName.equalsIgnoreCase("Canada") || countryName.equalsIgnoreCase("CA"))) {
        } else {
            toNumber = getSimpleFormattedUSPhoneNumber(toNumber);
        }
        // CallCreator callCreator = null;
        if (enableMachineAnsweredDetection && useSlice && disableAMDOnQueued && callRetryCount > 0 && dataSlice != null
                && !dataSlice.equalsIgnoreCase("slice0") && !dataSlice.equalsIgnoreCase("slice10")) {
            /*
             * AMD on data which is not in slice0, slice10 & Queued --
             * enableMachineAnsweredDetection is true, useSlice is true, disableAMDOnQueued
             * is true, callRetryCount > 0 (i.e. 2nd retry) & dataSlice is slice1
             */
            // callCreator = plivoInitiateAMDCall(callCreator, pcid, fromNumber, toNumber,
            // answerUrl, callTimeLimit,
            // campaignDTO, sipHeaders);
        } else if (enableMachineAnsweredDetection && useSlice && !disableAMDOnQueued && dataSlice != null
                && !dataSlice.equalsIgnoreCase("slice0") && !dataSlice.equalsIgnoreCase("slice10")) {
            /*
             * AMD on data which is not in slice0 & slice10 --
             * enableMachineAnsweredDetection is true, useSlice is true, disableAMDOnQueued
             * is false & dataSlice is slice1
             */
            // callCreator = plivoInitiateAMDCall(callCreator, pcid, fromNumber, toNumber,
            // answerUrl, callTimeLimit,
            // campaignDTO, sipHeaders);
        } else if (enableMachineAnsweredDetection && !useSlice) {
            /*
             * AMD on all calls -- enableMachineAnsweredDetection is true & useSlice is
             * false
             */
            // callCreator = plivoInitiateAMDCall(callCreator, pcid, fromNumber, toNumber,
            // answerUrl, callTimeLimit,
            // campaignDTO, sipHeaders);
        } else {
            // NO AMD
            // callCreator = Call.creator(fromNumber, Collections.singletonList(toNumber),
            // answerUrl)
            // .ringTimeout(maxCallRingingDuration).answerMethod("POST").hangupUrl(plivoAppHangupUrl)
            // .hangupMethod("POST").fallbackUrl(plivoAppFallbackUrl).sipHeaders(sipHeaders)
            // .timeLimit(callTimeLimit);
        }
        // callSid = callCreator.create().getRequestUuid();
        // } catch (IOException e) {
        // log.error("Exception occurred in Signalwire placeCall for pcid [{}]",
        // prospectCallDTO.getProspectCallId());
        // log.error("Exception is : {} ", e);
        // }
        Map<String, Object> dialCallResponse = null;
        // SignalwireCreateCallDTO request = new SignalwireCreateCallDTO();
        // request.setApplicationSid(signalwireApplicationSid);
        // request.setUrl("https://20eefcf1735f.ngrok.io/spr/signalwire/voice");

        // request.setUrl(xtaasBaseUrl + "/spr/signalwire/voice");
        // request.setFrom(fromNumber);
        // request.setTo(toNumber);
        // request.setRecord("true");
        // request.setStatusCallbackEvent("completed");
        // String statusCallbackUrl = answerUrl.replace("/voice","/voicestatus");
        // request.setStatusCallback(xtaasBaseUrl + "/spr/signalwire/voicestatus");
        URI uri = buildUri("/Calls.json");
        Long maxCallRingingDuration = cacheService.getLongApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION_AUTO_DIAL.name(), 21);
        map.add("To", toNumber);
        String voiceUrl = xtaasBaseUrl + "/spr/signalwire/voice?pcid=" + pcid;
        if (campaignDTO.isAutoIVR()) {
            voiceUrl += "&autoIVR=true";
            String extension = prospectCallDTO.getProspect().getExtension();
            if (extension != null && !extension.isEmpty()) {
                voiceUrl += "&defaultExtension=" + extension;
                log.info("SignalwireCallService =====> Default Extension : " + extension);
            } else {
                log.info("SignalwireCallService =====> Default Extension : Empty");
            }
        }        
        map.add("Url", voiceUrl);
        map.add("From", fromNumber);
        map.add("StatusCallbackEvent", "completed");
        map.add("StatusCallback", xtaasBaseUrl + "/spr/signalwire/voicestatus");
        map.add("Record", "true");
        map.add("RecordingStatusCallback", xtaasBaseUrl + "/spr/signalwire/recordingstatus?pcid=" + pcid);
        map.add("Timeout", Long.toString(maxCallRingingDuration));

        //for testing
        // log.info("input map ====>" + map.toString());
        // log.info("input uri ====>" + uri.toString());
        try {
            dialCallResponse = (Map<String, Object>) httpService.postUrlencoded(uri, headers(),
                    new ParameterizedTypeReference<Map<String, Object>>() {
                    }, map);
        } catch (Exception e) {
            log.error("Signalwire create call error :" + e.getMessage());
            // log.error("output response map =====> " + dialCallResponse.toString());
        }
        callSid = (String) dialCallResponse.get("sid");
        return callSid;
    }

    private static String getSimpleFormattedUSPhoneNumber(String phoneNumber) {
        if (phoneNumber == null) {
            return null;
        }
        String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d.]", ""); // removes all non-numeric chars
        String formattedPhoneNumber = normalizedPhoneNumber;
        if (normalizedPhoneNumber.length() == 10) {
            formattedPhoneNumber = "+1" + normalizedPhoneNumber;
        } else {
            formattedPhoneNumber = "+" + formattedPhoneNumber;
        }
        return formattedPhoneNumber;
    }

    private String createTelnyxNumber(String phoneNumber, String countryName) {
        String toNumber = "";
        if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
                || countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
                || countryName.equalsIgnoreCase("CA"))) {
            toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
        } else {
            toNumber = phoneNumber;
        }
        StringBuilder telnyxNumber = new StringBuilder("sip:").append(toNumber).append("@").append(telnyxDomainUrl)
                .append("?X-Telnyx-Token=").append(signalwireTelnyxToken);
        return telnyxNumber.toString();
    }

    private String createThinQNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber;
		}
        StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@").append(thinQDomainUrl)
				.append("?X-account-id=").append(thinQId).append("&X-account-token=").append(thinQToken);
        return thinQNumber.toString();
	}

    private String createPlivoSIPNumber(String phoneNumber, String countryName) {
        String toNumber = "";
        if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
                || countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
                || countryName.equalsIgnoreCase("CA"))) {
            toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
        } else {
            toNumber = phoneNumber;
        }
        StringBuilder plivoSIPNumber = new StringBuilder("sip:").append(toNumber).append("@").append(plivoSIPDomain);
        return plivoSIPNumber.toString();
    }

}
