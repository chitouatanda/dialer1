package com.xtaas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import com.xtaas.domain.TelnyxOutboundNumber;
import com.xtaas.service.dto.ProspectCallDTO;
import com.xtaas.service.util.PhoneNumberUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
public class TelnyxOutboundNumberService {

  private final Logger logger = LoggerFactory.getLogger(TelnyxOutboundNumberService.class);
	public static final String OUTBOUND_NUMBER_URL_TEMPLATE = "/spr/telnyx/outboundnumbers";
  private final XTaaSService xtaasService;

  private Map<String, List<TelnyxOutboundNumber>> telnyxOutboundNumbersMap = new HashMap<>();
  private Map<String, Integer> telnyxPoolMap = new HashMap<>();

  public TelnyxOutboundNumberService(XTaaSService xtaasService) {
		this.xtaasService = xtaasService;
  }

  public Boolean refreshOutboundNumbers() {
		if (telnyxOutboundNumbersMap != null) {
			telnyxOutboundNumbersMap.clear();
		}
		logger.info("Telnyx outbound number cache refreshed successully.");
		return true;
  }

	public String getTelnyxOutboundNumber(ProspectCallDTO prospectCallDTO, boolean localOutboundCalling,
			String organizationId) {
		List<TelnyxOutboundNumber> outboundNumbers = telnyxOutboundNumbersMap.get(organizationId);
		if (outboundNumbers == null || outboundNumbers.isEmpty()) {
			outboundNumbers = telnyxOutboundNumbersMap.get("COMMON_POOL");
			if (outboundNumbers == null || outboundNumbers.isEmpty()) {
				cacheAllTelnyxOutboundNumber();
				outboundNumbers = telnyxOutboundNumbersMap.get(organizationId);
				if (outboundNumbers == null || outboundNumbers.isEmpty()) {
					outboundNumbers = telnyxOutboundNumbersMap.get("COMMON_POOL");
					telnyxOutboundNumbersMap.put(organizationId, outboundNumbers);
				}
				cacheTelnyxNumbersPoolDetails(outboundNumbers);
			}
		}
		String number = getOutboundNumber(prospectCallDTO, localOutboundCalling, organizationId, outboundNumbers,
				telnyxPoolMap);
		return number;
	}

  private String getOutboundNumber(ProspectCallDTO prospectCallDTO, boolean localOutboundCalling,
			String organizationId, List<TelnyxOutboundNumber> outboundNumbers, Map<String, Integer> poolMap) {
		List<TelnyxOutboundNumber> outboundNumberList = new ArrayList<>();
		String phoneNumber = prospectCallDTO.getProspect().getPhone().replaceAll("[^\\d]", "");
		String countryName = prospectCallDTO.getProspect().getCountry();
		int maxPoolSize = 2;
		String key = organizationId + "_" + countryName;
		if (countryName != null && poolMap.get(key) != null) {
			maxPoolSize = poolMap.get(key);
		}
		int tempMaxPool = maxPoolSize;
		int pool = prospectCallDTO.getCallRetryCount() % maxPoolSize;
		int areaCode = Integer.parseInt(PhoneNumberUtil.findAreaCode(phoneNumber));
		if (pool == 0) {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
						&& number.getCountryName().equalsIgnoreCase(countryName)).collect(Collectors.toList());
			}
		} else {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == pool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(
						number -> number.getPool() == pool && number.getCountryName().equalsIgnoreCase(countryName))
						.collect(Collectors.toList());
			}
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers.stream()
					.filter(number -> number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers;
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = telnyxOutboundNumbersMap.get("COMMON_POOL");
		}

		TelnyxOutboundNumber outboundNumber = outboundNumberList.get(new Random().nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
  }

  private void cacheAllTelnyxOutboundNumber() {
	  	logger.info("cacheAllTelnyxOutboundNumber() :: Fetching telnyxoutboundnumbers from DB to cache.");
		List<TelnyxOutboundNumber> outboundNumbers = xtaasService.getAsList(OUTBOUND_NUMBER_URL_TEMPLATE,
				new ParameterizedTypeReference<TelnyxOutboundNumber[]>() {
				});
		telnyxOutboundNumbersMap = outboundNumbers.stream()
				.filter(number -> number.getProvider() != null && number.getProvider().equalsIgnoreCase("TELNYX"))
				.collect(Collectors.groupingBy(TelnyxOutboundNumber::getPartnerId));
	}

  private void cacheTelnyxNumbersPoolDetails(List<TelnyxOutboundNumber> outboundNumbers) {
		for (TelnyxOutboundNumber outboundNumber : outboundNumbers) {
			String key = outboundNumber.getPartnerId() + "_" + outboundNumber.getCountryName();
			if (telnyxPoolMap.get(key) == null) {
				telnyxPoolMap.put(key, outboundNumber.getPool());
			} else if (telnyxPoolMap.get(key) < new Integer(outboundNumber.getPool())) {
				telnyxPoolMap.put(key, outboundNumber.getPool());
			}
		}
	}

}