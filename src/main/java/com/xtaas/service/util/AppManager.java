package com.xtaas.service.util;

import org.apache.commons.lang3.EnumUtils;

public class AppManager {

	// Secret Key used for Hash generation for Rest API Authentication purpose
	public static final String SECRET_KEY_DIALER = "OE8QFDEAKD39108JBSYAUSQ8A32JNWA1";
	private static final String SECRET_KEY_BACKEND_SUPPORT_SYSTEM = "WE4EACFHHY54632RFCKDHNY6F35HMJF6";

	public enum App {
		DIALER, DIALER_PREPROCESS, BACKEND_SUPPORT_SYSTEM;
	}

	public static String getSecurityKey(App app) {
		String secretKey = null;
		switch (app) {
		case DIALER:
			secretKey = SECRET_KEY_DIALER;
			break;
		case BACKEND_SUPPORT_SYSTEM:
			secretKey = SECRET_KEY_BACKEND_SUPPORT_SYSTEM;
			break;
		}
		return secretKey;
	}

	public static String getSecurityKey(String appName) {
		return AppManager.getSecurityKey(EnumUtils.getEnum(App.class, appName));
	}
}
