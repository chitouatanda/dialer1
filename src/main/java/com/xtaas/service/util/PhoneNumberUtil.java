package com.xtaas.service.util;

public class PhoneNumberUtil {
	public static String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replaceAll("[^\\d.]", ""); // phone.replace("-", "");
		int length = removeSpclChar.length();
		/*
		 * handled phone numbers with length less than 10 digit & return first two digit
		 * as area code - e.g. Germany: +494023750
		 */
		if (length < 10) {
			return removeSpclChar.substring(0, 2);
		}
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = removeSpclChar.substring(startPoint, endPoint);
		return areaCode;
	}
}
