package com.xtaas.service.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class LogUtil {
	public static String extractStackTrace(Throwable ex) {
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}
}