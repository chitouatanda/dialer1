package com.xtaas.service.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	/**
	 * Gets today date without any time in it
	 */
	public static Date getToday() {
		Date today = java.sql.Date.valueOf(LocalDate.now());
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		return cal.getTime();
	}
	
	
	public static Date getNow() {
		return new Date();
	}
	
	public static Date getAtTimestamp(long timestamp) {
		return new Date(timestamp);
	}
	
	public static Date getAtTime(int hours, int minutes, int seconds, int milliSeconds) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, hours);
		cal.set(Calendar.MINUTE, minutes);
		cal.set(Calendar.SECOND, seconds);
		cal.set(Calendar.MILLISECOND, milliSeconds);
		return cal.getTime();
	}
	
	public static Date getDateAtTime(Date date, int hours, int minutes, int seconds, int milliSeconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, hours);
		cal.set(Calendar.MINUTE, minutes);
		cal.set(Calendar.SECOND, seconds);
		cal.set(Calendar.MILLISECOND, milliSeconds);
		return cal.getTime();
	}
	
	
//	public static Date adjustToToday(Date referenceDate) {
//		Calendar today = Calendar.getInstance();
//		Calendar adjustedDate = Calendar.getInstance();
//		adjustedDate.setTime(referenceDate);
//		adjustedDate.set(Calendar.DATE, today.get(Calendar.DATE));
//		adjustedDate.set(Calendar.MONTH, today.get(Calendar.MONTH));
//		adjustedDate.set(Calendar.YEAR, today.get(Calendar.YEAR));
//		
//        return adjustedDate.getTime();
//	}
	
	public static Date resetDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}
	
	public static Date adjustToDate(Date referenceDate, Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Calendar adjustedDate = Calendar.getInstance();
		adjustedDate.setTime(referenceDate);
		adjustedDate.set(Calendar.DATE, cal.get(Calendar.DATE));
		adjustedDate.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		adjustedDate.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		
        return adjustedDate.getTime();
	}
	
	public static Date adjustDateToReference(Date date, Date reference) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(reference);
		Calendar adjustedDate = Calendar.getInstance();
		adjustedDate.setTime(date);
		adjustedDate.set(Calendar.DATE, cal.get(Calendar.DATE));
		adjustedDate.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		adjustedDate.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		
        return adjustedDate.getTime();
	}
	
	public static Date adjustTimeToReference(Date date, Date reference) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(reference);
		Calendar adjustedDate = Calendar.getInstance();
		adjustedDate.setTime(date);
		adjustedDate.set(Calendar.HOUR, cal.get(Calendar.HOUR));
		adjustedDate.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
		adjustedDate.set(Calendar.SECOND, cal.get(Calendar.SECOND));
		adjustedDate.set(Calendar.MILLISECOND, cal.get(Calendar.MILLISECOND));
		
        return adjustedDate.getTime();
	}
	
	
	public static Date getLastThursday(int year, int month) {
		return java.sql.Date.valueOf(LocalDate.of(year, month, 1).with(TemporalAdjusters.lastInMonth(DayOfWeek.THURSDAY)));
	}
	
	public static Date getLastThursday() {
		return java.sql.Date.valueOf(LocalDate.now().with(TemporalAdjusters.lastInMonth(DayOfWeek.THURSDAY)));
	}
	
	public static boolean isTodaySunday() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
	}
	
	public static boolean isTodaySaturday() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY;
	}
}