/**
 * 
 */
package com.xtaas.service.util;

import java.util.Arrays;
import java.util.List;

/**
 * @author djain
 *
 */
public interface XtaasConstants {

	public static final String NEWLINE = System.getProperty("line.separator");

	public static final Long ONE_HOUR_IN_MILLIS = new Long(3600000);

	public static final Long ONE_DAY_IN_MILLIS = new Long(86400000);

	public static final Long MILLIS_IN_ONE_SEC = new Long(1000);

	public static final String SYSTEM_USERNAME = "SYSTEM";

	public static final String UTF8 = "UTF-8";

	public static final String PACIFIC_TZ = "US/Pacific";

	// Enum of all Application properties stored in "applicationproperty" collection
	public static enum APPLICATION_PROPERTY {
		CHECK_PHONENUMBER_USING_STRIKEIRON, // Flag to control whether to verify PhoneNumber and DNC using StrikeIron
											// APIs
		CALL_NO_ANSWER_CALLBACK_HRS, // Callback after specified hrs for call with no-answer status
		CALL_BUSY_CALLBACK_HRS, // Callback after specified hrs for call with busy status
		CALL_MAX_RETRIES, // Max number of retries for calls with no-answer and busy status
		DIALER_THREAD_COUNT, // Count of Dialer threads running in dialer component
		BATCH_JOB_NOTIFICATION_EMAILS, // Email addresses to whom batch job events (success/failure) needs to be sent
		OUTBOUND_CALL_HANGUP_ON_AGENT_DISCONNECTION, // Hangup Outbound call initiated by agent when agent hit
														// disconnect on dialer pad
		MAX_CALL_RINGING_DURATION_AUTO_DIAL, // Max duration till when a call can be in ringing state
		ENABLE_MACHINE_ANSWERED_DETECTION, // Enable check for detecting if call is answered by machine
		DAILY_CALL_MAX_RETRIES, // Daily call limit to a prospect
		MAX_CALL_RETRY_COUNT_MACHINE_DETECTION, // enable machine detection if prospect's callRetry count is greater
												// than equals to this count.
		MAX_PROSPECT_VERSION, // Added to eliminate duplicate leads in running campaigns.
		QA_PORTAL_ACCESS, MAX_DIALERCODE_LIMIT, MAX_MULTIPROSPECT_DIALERCODE_LIMIT, CALLBACK_MINUTES,
		MAX_CALL_RINGING_DURATION_MANUALDIAL, PROSPECT_CALL_DETAILS_UPDATE_THREAD_INSTANCE,
		FAILURE_RECORDINGS_DOWNLOAD_THREAD_INSTANCE, TWILIO_CALL_LOG_JOB_INSTANCE, ZOOM_TOKEN_THREAD_INSTANCE,
		DATA_BUY_THREAD_INSTANCE, CACHE_MODE, AGENT_ABSENT_DAYS, AGENT_DEACTIVATE_JOB_INSTANCE,
		PLIVO_MAX_CALL_TIME_LIMIT, PLIVO_AMD_TYPE, PLIVO_AMD_TIME, PARTNER_NOT_SCORED_JOB_INSTANCE
	};

	// TWILIO CONSTANTS
	public static final String TWILIO = "TWILIO";

	// Twilio Call Statuses
	public static final String TWILIO_CALL_STATUS_QUEUED = "queued";
	public static final String TWILIO_CALL_STATUS_RINGING = "ringing";
	public static final String TWILIO_CALL_STATUS_INPROGRESS = "in-progress";
	public static final String TWILIO_CALL_STATUS_COMPLETED = "completed";
	public static final String TWILIO_CALL_STATUS_BUSY = "busy";
	public static final String TWILIO_CALL_STATUS_FAILED = "failed";
	public static final String TWILIO_CALL_STATUS_NO_ANSWER = "no-answer";
	public static final String TWILIO_CALL_STATUS_CANCELED = "canceled";
	public static final String TWILIO_CALL_MACHINE_ANSWERED = "machine";

	// Twilio IVR messages
	public static final String TWILIO_ABANDON_OPTOUT_MSG = "Sorry, No agent available. Press 1 followed by pound sign to opt out";
	public static final String TWILIO_VM_LEFT_BY_DIALER = "Hi, We at XTaaS were trying to reach you. Please call us back at 415-403-2426";

	// Webhook Email Bounce Regx constant
	public static final String FEEDBACK_ID = "(?m)Feedback-ID.*";
	public static final String FROM_EMAIL = "(?m)From:.*";
	public static final String ACTION = "(?m)Action:.*";
	public static final String STATUS = "(?m)Status:.*";
	public static final String EMAIL_BOUNCE_MESSAGE_ID = "(?m)Message-ID:.*";

	public static final String INSIDEUP_CLIENT = "INSIDEUP";
	public static final String WHEELHOUSE_CLIENT = "WHEELHOUSE";

	public static enum LEAD_STATUS {
		NOT_SCORED, QA_ACCEPTED, QA_REJECTED, QA_RETURNED, ACCEPTED, REJECTED, CLIENT_ACCEPTED, CLIENT_REJECTED
	}

	public static final String PROSPECT_CALL_EVENT = "prospect_call";
	public static final String CALL_DISCONNECCT_EVENT = "call_disconnect";
	public static final String AGENT_TYPE_CHANGE = "agent_type_change";
	public static final String SCRIPT_CHANGE = "script_change";
	public static final String CAMPAIGN_SETTINGS_CHANGE = "campaign_settings_change";

	// Pivots checkPoints
	public static final String MANAGEMENT_LEVELS = "MANAGEMENT LEVELS";
	public static final String DEPARTMENT = "DEPARTMENT";
	public static final String EMPLOYEE_COUNT = "EMPLOYEE COUNT";
	public static final String REVENUE = "REVENUE";
	public static final String INDUSTRY = "INDUSTRY";
	public static final String TITLE = "TITLE";
	public static final String COUNTRY = "COUNTRY";
	public static final String COMPANY = "COMPANY";
	public static final String SOURCE = "SOURCE";
	public static final String DNC_COMPANY = "COMPANY";
	public static final String DNC_PROSPECT = "PROSPECT";

	public static final String SOFT_DELETE = "DELETE";
	public static final String HARD_DELETE = "REMOVE";

	public static final String DEMANDSHORE = "DEMANDSHORE";

	public static final String CALL_RECORDING_AWS_URL = "https://s3-us-west-2.amazonaws.com/xtaasrecordings/recordings/";

	public static final String XTAAS_RECORDING_BUCKET_NAME = "xtaasrecordings/recordings";
	public static final String S3_FILE_URL_TEMPLATE = "https://s3-us-west-2.amazonaws.com/%s/%s";

	public static final String DIRECT_PHONE = "DIRECT";

	public static final String INDIRECT_PHONE = "INDIRECT";

	public static enum PROSPECT_TYPE {
		POWER, PREVIEW, RESEARCH
	}

	public static enum ProspectCallStatus {
		QUEUED, LOCAL_DNC_ERROR, NATIONAL_DNC_ERROR, UNKNOWN_ERROR, MAX_RETRY_LIMIT_REACHED, CALL_TIME_RESTRICTION,
		CALL_NO_ANSWER, CALL_BUSY, CALL_CANCELED, CALL_FAILED, CALL_MACHINE_ANSWERED, CALL_ABANDONED, CALL_PREVIEW,
		CALL_DIALED, CALL_INPROGRESS, CALL_COMPLETE, WRAPUP_COMPLETE, QA_INITIATED, QA_COMPLETE, SECONDARY_QA_INITIATED,
		QA_WRAPUP_COMPLETE, CLIENT_DELIVERED, CLIENT_REJECTED, QA_REJECTED, PRIMARY_REVIEW, SECONDARY_REVIEW,
		INDIRECT_CALLING, SUCCESS_DUPLICATE
	}

	public static final String XTAASCORP_URL = "https://xtaascorp.herokuapp.com";
	public static final String XTAASCORP2_URL = "https://demandupgrade.herokuapp.com";
	public static final String DEMANDSHORE_URL = "https://demandshore.herokuapp.com";
	public static final String PEACEFUL_URL = "https://peaceful-castle-6000.herokuapp.com";

	public final String CLIENT_REGION = "us-west-2";
	public final String AWS_ACCESS_KEY_ID = "AKIAJZ5EWMT6OAKGOGNQ";
	public final String AWS_SECRET_KEY = "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";

	public static final List<String> slices = Arrays.asList("slice0", "slice1", "slice2", "slice3", "slice4", "slice5",
			"slice6", "slice10");
	// public static final List<String> slices = Arrays.asList("slice0", "slice1",
	// "slice2", "slice3", "slice4", "slice5", "slice6", "slice7", "slice8",
	// "slice9", "slice10", "slice11", "slice12");
	public static final List<String> indirect_slices = Arrays.asList("slice0", "slice3", "slice4", "slice9", "slice10",
			"slice11", "slice12");

	public final String CALLABLE_COUNT = "CALLABLE_COUNT";

	// PLIVO CONSTANSTS
    public static final String PLIVO = "PLIVO";
    public static final String PLIVO_SYNC_AMD_URL = "/spr/plivo/machinedetection";
	// Plivo Endpoint
	public static final String PLIVO_ENDPOINT_USERNAME = "PLIVO_ENDPOINT1";
	public static final String PLIVO_ENDPOINT_PASSWORD = "PLIVO_ENDPOINT2";
	public static final String PLIVO_ENDPOINT_ALIAS = "PLIVO_ENDPOINT_ALIAS";
	public static final String PLIVO_ENDPOINT_ID = "PLIVO_ENDPOINT_ID";

	// Plivo IVR messages
	public static final String PLIVO_ABANDON_OPTOUT_MSG = "Sorry, No agent available. Press 1 followed by pound sign to opt out";
	public static final String PLIVO_VM_LEFT_BY_DIALER = "Hi, We at XTaaS were trying to reach you. Please call us back at 415-403-2426";

	public static final String FILE_TYPE_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	//ENVIRONMENT Variable keys
	public static final String DIALER_XTAAS_BASE_URL = "DIALER_XTAAS_BASE_URL";
	public static final String DIALER_AUTHORIZATION = "DIALER_AUTHORIZATION";
	public static final String DIALER_MAX_POOL_SIZE = "DIALER_MAX_POOL_SIZE";
	public static final String DIALER_MACHINEDETECTION_CALL_RETRY_COUNT = "DIALER_MACHINEDETECTION_CALL_RETRY_COUNT";
	public static final String TWILIO_ACCOUNT_SID = "TWILIO_ACCOUNT_SID";
	public static final String TWILIO_AUTH_TOKEN = "TWILIO_AUTH_TOKEN";
	public static final String TWILIO_APP_URL = "TWILIO_APP_URL";
	public static final String TWILIO_RING_TIMEOUT = "TWILIO_RING_TIMEOUT";
	public static final String TWILIO_FROM_NUMBER = "TWILIO_FROM_NUMBER";
	public static final String TWILIO_CALLSTATUS_CALLBACK = "TWILIO_CALLSTATUS_CALLBACK";
	public static final String THINQ_DOMAIN_URL = "THINQ_DOMAIN_URL";
	public static final String THINQ_ID = "THINQ_ID";
	public static final String THINQ_TOKEN = "THINQ_TOKEN";
	public static final String PLIVO_APPURL = "PLIVO_APPURL";
	public static final String PLIVO_APP_HANGUP_URL = "PLIVO_APP_HANGUP_URL";
	public static final String PLIVO_APP_FALLBACK_URL = "PLIVO_APP_FALLBACK_URL";
	public static final String PLIVO_OUTBOUNDNUMBER = "PLIVO_OUTBOUNDNUMBER";
	public static final String PLIVO_AUTH_ID = "PLIVO_AUTH_ID";
	public static final String PLIVO_AUTH_TOKEN = "PLIVO_AUTH_TOKEN";
	public static final String PLIVO_APP_ID = "PLIVO_APP_ID";
	public static final String PLIVO_SIP_DOMAIN = "PLIVO_SIP_DOMAIN";
	public static final String PLIVO_SIP_USERNAME = "PLIVO_SIP_USERNAME";
	public static final String PLIVO_SIP_PASSWORD = "PLIVO_SIP_PASSWORD";
	public static final String TELNYX_DOMAIN_URL = "TELNYX_DOMAIN_URL";
	public static final String TELNYX_TOKEN = "TELNYX_TOKEN";
	public static final String TELNYX_BASE_APIURL = "TELNYX_BASE_APIURL";
	public static final String TELNYX_CONNECTION_ID = "TELNYX_CONNECTION_ID";
	public static final String VIVA_DOMAIN = "VIVA_DOMAIN";
	public static final String VIVA_ACCOUNTID = "VIVA_ACCOUNTID";
	public static final String VIVA_PASSCODE = "VIVA_PASSCODE";
	public static final String VIVA_TELNYX_DOMAIN = "VIVA_TELNYX_DOMAIN";
	public static final String VIVA_TELNYX_ACCOUNTID = "VIVA_TELNYX_ACCOUNTID";
	public static final String VIVA_TELNYX_PASSCODE = "VIVA_TELNYX_PASSCODE";
	public static final String SAMESPACE_DOMAIN = "SAMESPACE_DOMAIN";
	public static final String SAMESPACE_USERNAME = "SAMESPACE_USERNAME";
	public static final String SAMESPACE_PASSWORD = "SAMESPACE_PASSWORD";
	public static final String SIGNALWIRE_PROJECTID = "SIGNALWIRE_PROJECTID";
	public static final String SIGNALWIRE_APPLICATIONSID = "SIGNALWIRE_APPLICATIONSID";
	public static final String SIGNALWIRE_AUTH_TOKEN = "SIGNALWIRE_AUTH_TOKEN";
	public static final String SIGNALWIRE_BASE_URL = "SIGNALWIRE_BASE_URL";
	public static final String SIGNALWIRE_TELNYX_TOKEN = "SIGNALWIRE_TELNYX_TOKEN";
	public static final String SIGNALWIRE_THINQ_TOKEN = "SIGNALWIRE_THINQ_TOKEN";

}
