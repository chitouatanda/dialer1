package com.xtaas.service.util;

import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class EncryptionUtils {
	private static final Logger logger = LoggerFactory.getLogger(EncryptionUtils.class);

	private static final String SECRET_KEY = "ZX3DSASKDK98943JKDJSKAD9E23NMXD4";

	private static final String AES = "AES";

	/**
	 * Computes an RFC 2104-compliant HMAC signature for an array of bytes and
	 * returns the result as a Base64 encoded string.
	 * 
	 * Algos supported:
	 * http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Mac
	 */
	public static String generateHash(String data, String secretKey) {
		try {
			Mac sha256HMAC = Mac.getInstance("HmacSHA256");

			sha256HMAC.init(new SecretKeySpec(secretKey.getBytes(), "HmacSHA256"));
			return Base64.encodeBase64String(sha256HMAC.doFinal(data.getBytes()));
		} catch (Exception e) {
			logger.error("generateHash(): Error occurred while generating a Hash.", e);
			return data;
		}
	}

	public static String generateMd5Hash(String data) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes());
			return Base64.encodeBase64String(md.digest());
		} catch (Exception e) {
			logger.error("generateHash(): Error occurred while generating a Hash.", e);
			return data;
		}
	}

	/**
	 * Two way encryption
	 * 
	 * @param stringIn
	 * @return
	 */
	public static String encrypt(String plainText) {
		return encrypt(plainText, SECRET_KEY);
	}

	public static String encrypt(String plainText, String secretKey) {
		try {
			Key key = generateKey(secretKey);

			Cipher cipher = Cipher.getInstance(AES);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encVal = cipher.doFinal(plainText.getBytes());

			return Base64.encodeBase64String(encVal);
		} catch (Exception e) {
			logger.error("Exception during encryption.", e);
			return plainText;
		}
	}

	public static String decrypt(String encryptedText) {
		return decrypt(encryptedText, SECRET_KEY);
	}

	public static String decrypt(String encryptedText, String secretKey) {
		try {
			Key key = generateKey(secretKey);

			// Instantiate the cipher
			Cipher cipher = Cipher.getInstance(AES);
			cipher.init(Cipher.DECRYPT_MODE, key);

			byte[] encryptedTextBytes = Base64.decodeBase64(encryptedText);
			byte[] decryptedTextBytes = cipher.doFinal(encryptedTextBytes);

			return new String(decryptedTextBytes);
		} catch (Exception e) {
			logger.error("Exception during decryption.", e);
			return encryptedText;
		}
	}

	private static Key generateKey(String secretKey) throws Exception {
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		byte[] keyBytes = sha.digest(secretKey.getBytes());
		keyBytes = Arrays.copyOf(keyBytes, 16); // use only first 128 bit

		Key key = new SecretKeySpec(keyBytes, AES);
		return key;
	}

}
