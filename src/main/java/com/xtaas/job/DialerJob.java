package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xtaas.Task.DialerTask;
import com.xtaas.service.util.LogUtil;

@Component
public class DialerJob {

	private final Logger log = LoggerFactory.getLogger(DialerJob.class);

	private final DialerTask dialerTask;

	public DialerJob(DialerTask dialerTask) {
		this.dialerTask = dialerTask;
	}

	@Scheduled(cron = "*/5 * * * * *", zone = "CET")
	public void performDialing() {
		try {
			log.debug("Starting the dialer job");
			dialerTask.processCampaigns();
		} catch (Throwable e) {
			log.error("Failed to run the dialer job {}", e.getMessage());
			log.info(LogUtil.extractStackTrace(e));
		}
	}
}
