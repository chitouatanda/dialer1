package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xtaas.Task.TwilioRecordingsDeleterTask;
import com.xtaas.service.util.LogUtil;

@Component
public class TwilioRecordingsDeleterJob {

	private final Logger log = LoggerFactory.getLogger(TwilioRecordingsDeleterJob.class);

	private final TwilioRecordingsDeleterTask twilioRecordingsDeleterTask;

	public TwilioRecordingsDeleterJob(TwilioRecordingsDeleterTask twilioRecordingsDeleterTask) {
		this.twilioRecordingsDeleterTask = twilioRecordingsDeleterTask;
	}

	@Scheduled(cron = "0 0 17 * * MON-FRI", zone = "PST")
	public void deleteRecordings() {
		try {
			log.info("Starting the twilio recordings deleter job.");
			twilioRecordingsDeleterTask.deleteRecordings();
		} catch (Throwable e) {
			log.error("Failed to run the twilio recordings deleter job {}", e.getMessage());
			log.info(LogUtil.extractStackTrace(e));
		}
	}

}
