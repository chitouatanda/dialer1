/**
 * View Models used by Spring MVC REST controllers.
 */
package com.xtaas.web.rest.vm;
