package com.xtaas.web.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.xtaas.service.CallService;

@RestController
@RequestMapping("/api")
public class CallResource {
	private final Logger log = LoggerFactory.getLogger(CallResource.class);

	private final CallService callService;

	public CallResource(CallService callService) {
		this.callService = callService;
	}

	/**
	 * Twilio status callback
	 *
	 * @param callSid
	 * @param callStatus
	 * @return
	 */
	@PostMapping(value = "/call/status", produces = "application/xml")
	@Timed
	public ResponseEntity<?> updateCallStatus(@RequestParam("CallSid") String callSid,
			@RequestParam("CallStatus") String callStatus, HttpServletResponse response, HttpServletRequest request) {
		log.info("Received call status update for callsid: {}, status: {}", callSid, callStatus);
		callService.processCallStatus(callSid, callStatus, request.getParameter("AnsweredBy"),
				request.getParameter("MachineAnsweredDetection"));
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	/**
	 * Plivo hangup callback
	 *
	 * @return
	 */
	@PostMapping(value = "/plivo/call/hangup", produces = "application/xml")
	@Timed
	public ResponseEntity<?> plivoCallHangup(HttpServletResponse response, HttpServletRequest request) {
		log.info("PLIVO - Received call status update for callsid: {}, status: {}", request.getParameter("CallUUID"),
				request.getParameter("CallStatus"));
		callService.processPlivoCallStatus(request, response);
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@GetMapping(value = "/plivo/call/status/{callSid}")
	public ResponseEntity<Boolean> removeAnsweredCallSid(@PathVariable String callSid) {
		log.debug("Request to remove CallSid [{}] from dialer cache.", callSid);
		callService.removeAnsweredCallSid(callSid);
		return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
	}

	@GetMapping(value = "/telnyx/call/status/{callSid}")
	public ResponseEntity<Boolean> removeTelnyxAnsweredCallSid(@PathVariable String callSid) {
		log.debug("Request to remove CallSid [{}] from dialer cache.", callSid);
		callService.removeTelnyxCallSid(callSid);
		return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
	}

	@GetMapping(value = "/signalwire/call/status/{callSid}")
	public ResponseEntity<Boolean> removeSignalwireAnsweredCallSid(@PathVariable String callSid) {
		log.debug("Request to remove CallSid [{}] from dialer cache.", callSid);
		callService.removeSignalwireAnsweredCallSid(callSid);
		return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
	}

}
