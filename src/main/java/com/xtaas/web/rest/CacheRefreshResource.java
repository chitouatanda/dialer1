package com.xtaas.web.rest;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.Task.DialerTask;
import com.xtaas.service.CacheService;
import com.xtaas.service.OutboundNumberService;
import com.xtaas.service.PlivoOutboundNumberService;
import com.xtaas.service.SignalwireOutboundNumberService;
import com.xtaas.service.TelnyxOutboundNumberService;

@RestController
@RequestMapping("/api")
public class CacheRefreshResource {

	private final Logger logger = LoggerFactory.getLogger(CacheRefreshResource.class);
	private final DialerTask dialerTask;
	private final CacheService cacheService;
	private final OutboundNumberService outboundNumberService;
	private final PlivoOutboundNumberService plivoOutboundNumberService;
	private final TelnyxOutboundNumberService telnyxOutboundNumberService;
	private final SignalwireOutboundNumberService signalwireOutboundNumberService;

	public CacheRefreshResource(DialerTask dialerTask, CacheService cacheService,
			OutboundNumberService outboundNumberService, PlivoOutboundNumberService plivoOutboundNumberService, TelnyxOutboundNumberService telnyxOutboundNumberService, SignalwireOutboundNumberService signalwireOutboundNumberService) {
		this.dialerTask = dialerTask;
		this.cacheService = cacheService;
		this.outboundNumberService = outboundNumberService;
		this.plivoOutboundNumberService = plivoOutboundNumberService;
		this.telnyxOutboundNumberService = telnyxOutboundNumberService;
		this.signalwireOutboundNumberService = signalwireOutboundNumberService;
	}

	@GetMapping("/cacherefresh/campaign/{campaignIds}")
	public boolean refreshCampaignCacheByCampaignId(@PathVariable(value = "campaignIds") String[] campaignIds) {
		logger.info("***** Campaign cache refresh started for [{}] campaignId *****", campaignIds);
		return dialerTask.refreshCampaignCacheByCampaignId(Arrays.asList(campaignIds));
	}

	@GetMapping("/cacherefresh/activecampaigns")
	public boolean refreshAllCampaignCache() {
		logger.info("***** Campaign cache refresh started for all active campaigns *****");
		return dialerTask.refreshAllCampaignCache();
	}

	@GetMapping("/cacherefresh/plivo/outboundnumbers")
	public boolean refreshPlivoOutboundNumbers() {
		logger.info("Request to refresh Plivo outbound number cache.");
		return plivoOutboundNumberService.refreshOutboundNumbers();
	}
	
	@GetMapping("/cacherefresh/telnyx/outboundnumbers")
	public boolean refreshTelnyxOutboundNumbers() {
		logger.info("Request to refresh Telnyx outbound number cache.");
		return telnyxOutboundNumberService.refreshOutboundNumbers();
	}

	@GetMapping("/cacherefresh/signalwire/outboundnumbers")
	public boolean refreshSignalwireOutboundNumbers() {
		logger.info("Request to refresh Signalwire outbound number cache.");
		return signalwireOutboundNumberService.refreshOutboundNumbers();
	}

	@GetMapping("/cacherefresh/twilio/outboundnumbers")
	public boolean refreshTwilioOutboundNumbers() {
		logger.info("Request to refresh Twilio outbound number cache.");
		return outboundNumberService.refreshOutboundNumbers();
	}

	@GetMapping("/cacherefresh/applicationproperty")
	public boolean loadApplicationPropertyCacheForcefully() {
		logger.info("Request to refresh application properties.");
		return cacheService.loadApplicationPropertyCacheForcefully();
	}

}
