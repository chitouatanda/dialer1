package com.xtaas.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.xtaas.service.TwilioRecordingService;
import com.xtaas.service.dto.RecordingFilterDTO;

@RestController
@RequestMapping("/api")
public class TwilioResource {

	private final Logger log = LoggerFactory.getLogger(TwilioResource.class);
	private final TwilioRecordingService twilioRecordingService;

	public TwilioResource(TwilioRecordingService twilioRecordingService) {
		this.twilioRecordingService = twilioRecordingService;
	}

	@Timed
	@PostMapping("/twilio/recording/delete")
	public void deleteRecordings(@RequestBody RecordingFilterDTO recordingFilterDTO) {
		log.info("Request to delete twilio recordings for [{}] .", recordingFilterDTO.toString());
		twilioRecordingService.deleteRecordings(recordingFilterDTO);
	}

	@Timed
	@GetMapping("/twilio/reset/phonemessagewebhook")
	public void resetMessageWebhook() {
		log.info("Request to reset twilio message webhook.");
		twilioRecordingService.resetMessageWebhook();
	}

}
