package com.xtaas.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.domain.OutboundNumber;

public interface OutboundNumberRepository extends MongoRepository<OutboundNumber, String> {
	List<OutboundNumber> findAllByBucketNameAndAreaCode(String bucketName, int areaCode);
	List<OutboundNumber> findAllByBucketName(String bucketName);
}
