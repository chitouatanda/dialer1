package com.xtaas.logging;

public enum SplunkEvent {

	EVENT_DESCRIPTION("eventDescription"), PROCESS_NAME("processName"), METHOD_NAME("methodName"),
	CAMPAIGN_ID("campaignId"), CAMPAIGN_NAME("campaignName"), AGENT_ID("agentId"), AGENT_NAME("agentName"),
	ORGANIZATION_ID("organizationId"), PROSPECT_CALL_ID("prospectCallId"),
	PROSPECT_INTERACTION_SESSION_ID("prospectInteractionSessionId"), CALL_SID("callSid"), CALL_STATE("callState"),
	CALL_DURATION_TIME("callDurationTime"), CALLABLE_DATA_TIMEZONE("callableDataTimezone"), DATA_SOURCE("dataSource"),
	OUTBOUND_NUMBER("outboundNumber"), TO_NUMBER("toNumber"), VOICE_PROVIDER("voiceProvider"), DATA_SLICE("dataSlice"),
	CALL_RETRY_COUNT("callRetryCount"), DAILY_CALL_RETRY_COUNT("dailyCallRetryCount"), STATUS("status"),
	DISPOSITION_STATUS("dispositionStatus"), SUB_STATUS("subStatus"), DIALER_MODE("dialerMode"),
	TIME_TAKEN("timeTaken"), DATA_RETRIVED("dataRetrived");

	private String value;

	SplunkEvent(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
