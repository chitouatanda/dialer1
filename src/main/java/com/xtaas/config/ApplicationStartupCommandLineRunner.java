package com.xtaas.config;

import com.xtaas.service.CacheService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartupCommandLineRunner implements CommandLineRunner {

    private final Logger log = LoggerFactory.getLogger(ApplicationStartupCommandLineRunner.class);
    private final CacheService cacheService;

    public ApplicationStartupCommandLineRunner(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Started ApplicationStartupCommandLineRunner");
        cacheService.loadApplicationPropertyCacheForcefully();
        log.info("Finished ApplicationStartupCommandLineRunner");
    }

}
