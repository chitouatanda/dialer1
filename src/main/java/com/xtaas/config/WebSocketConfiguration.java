package com.xtaas.config;

import com.xtaas.service.HttpService;
import com.xtaas.service.SocketHandler;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfiguration implements WebSocketConfigurer {

  private final HttpService httpService;

  public WebSocketConfiguration(HttpService httpService) {
      this.httpService = httpService;
  }

  public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
    registry.addHandler(new SocketHandler(httpService), "/audiostream").setAllowedOrigins("*");
  }

}