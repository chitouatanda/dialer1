package com.xtaas.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "en";

    private Constants() {
    }

    public static final String DIRECT_PHONE = "DIRECT";
    public static final String INDIRECT_PHONE = "INDIRECT";

    public static enum PROSPECT_TYPE {
        POWER, PREVIEW, RESEARCH
    }
}
