package com.xtaas.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Xtaas.
 * <p>
 * Properties are configured in the application.yml file. See
 * {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
// @ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
	private Dialer dialer = new Dialer();
	private final Twilio twilio = new Twilio();
	private final ThinQ thinQ = new ThinQ();
	private final Plivo plivo = new Plivo();
	private final Telnyx telnyx = new Telnyx();
	private final Viva viva = new Viva();
	private final Samespace samespace = new Samespace();
	private final Signalwire signalwire = new Signalwire();

	public class Dialer {
		private String xtaasBaseUrl = "";
		private String authorization = "";
		private int maxPoolSize = 1;
		private int machineDetectionCallRetryCount = 1;

		public String getXtaasBaseUrl() {
			return xtaasBaseUrl;
		}

		public void setXtaasBaseUrl(String xtaasBaseUrl) {
			this.xtaasBaseUrl = xtaasBaseUrl;
		}

		public String getAuthorization() {
			return authorization;
		}

		public void setAuthorization(String authorization) {
			this.authorization = authorization;
		}

		public int getMaxPoolSize() {
			return maxPoolSize;
		}

		public void setMaxPoolSize(int maxPoolSize) {
			this.maxPoolSize = maxPoolSize;
		}

		public int getMachineDetectionCallRetryCount() {
			return machineDetectionCallRetryCount;
		}

		public void setMachineDetectionCallRetryCount(int machineDetectionCallRetryCount) {
			this.machineDetectionCallRetryCount = machineDetectionCallRetryCount;
		}

	}

	public static class Twilio {
		private String accountSid = "";
		private String authToken = "";
		private String appUrl = "";
		private int ringTimeout = 0;
		private String fromNumber = "";
		private String callStatusCallback = "";

		public String getAccountSid() {
			return accountSid;
		}

		public void setAccountSid(String accountSid) {
			this.accountSid = accountSid;
		}

		public String getAuthToken() {
			return authToken;
		}

		public void setAuthToken(String authToken) {
			this.authToken = authToken;
		}

		public String getAppUrl() {
			return appUrl;
		}

		public void setAppUrl(String appUrl) {
			this.appUrl = appUrl;
		}

		public int getRingTimeout() {
			return ringTimeout;
		}

		public void setRingTimeout(int ringTimeout) {
			this.ringTimeout = ringTimeout;
		}

		public String getFromNumber() {
			return fromNumber;
		}

		public void setFromNumber(String fromNumber) {
			this.fromNumber = fromNumber;
		}

		public String getCallStatusCallback() {
			return callStatusCallback;
		}

		public void setCallStatusCallback(String callStatusCallback) {
			this.callStatusCallback = callStatusCallback;
		}

	}

	public static class ThinQ {
		private String domainUrl = "";
		private String id = "";
		private String token = "";

		public String getDomainUrl() {
			return domainUrl;
		}

		public void setDomainUrl(String domainUrl) {
			this.domainUrl = domainUrl;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

	}

	public Dialer getDialer() {
		return dialer;
	}

	public void setDialer(Dialer dialer) {
		this.dialer = dialer;
	}

	public Twilio getTwilio() {
		return twilio;
	}

	public ThinQ getThinQ() {
		return thinQ;
	}

	public Plivo getPlivo() {
		return plivo;
	}

	public Telnyx getTelnyx() {
		return telnyx;
	}

	public Viva getViva() {
		return viva;
	}
	
	public Samespace getSamespace() {
		return samespace;
	}

	public Signalwire getSignalwire() {
		return signalwire;
	}

	public static class Plivo {
		private String plivoAppUrl = "";
		private String plivoAppHangupUrl = "";
		private String plivoAppFallbackUrl = "";
		private String plivoOutboundNumber = "";
		private String plivoAuthId = "";
		private String plivoAuthToken = "";
		private String plivoAppId = "";
		private String plivoSIPDomain = "";
		private String plivoSIPUsername = "";
		private String plivoSIPPassword = "";

		public String getPlivoAppUrl() {
			return plivoAppUrl;
		}

		public void setPlivoAppUrl(String plivoAppUrl) {
			this.plivoAppUrl = plivoAppUrl;
		}

		public String getPlivoAppHangupUrl() {
			return plivoAppHangupUrl;
		}

		public void setPlivoAppHangupUrl(String plivoAppHangupUrl) {
			this.plivoAppHangupUrl = plivoAppHangupUrl;
		}

		public String getPlivoAppFallbackUrl() {
			return plivoAppFallbackUrl;
		}

		public void setPlivoAppFallbackUrl(String plivoAppFallbackUrl) {
			this.plivoAppFallbackUrl = plivoAppFallbackUrl;
		}

		public String getPlivoOutboundNumber() {
			return plivoOutboundNumber;
		}

		public void setPlivoOutboundNumber(String plivoOutboundNumber) {
			this.plivoOutboundNumber = plivoOutboundNumber;
		}

		public String getPlivoAuthId() {
			return plivoAuthId;
		}

		public void setPlivoAuthId(String plivoAuthId) {
			this.plivoAuthId = plivoAuthId;
		}

		public String getPlivoAuthToken() {
			return plivoAuthToken;
		}

		public void setPlivoAuthToken(String plivoAuthToken) {
			this.plivoAuthToken = plivoAuthToken;
		}

		public String getPlivoAppId() {
			return plivoAppId;
		}

		public void setPlivoAppId(String plivoAppId) {
			this.plivoAppId = plivoAppId;
		}

		public String getPlivoSIPDomain() {
			return plivoSIPDomain;
		}

		public void setPlivoSIPDomain(String plivoSIPDomain) {
			this.plivoSIPDomain = plivoSIPDomain;
		}

		public String getPlivoSIPUsername() {
			return plivoSIPUsername;
		}

		public void setPlivoSIPUsername(String plivoSIPUsername) {
			this.plivoSIPUsername = plivoSIPUsername;
		}

		public String getPlivoSIPPassword() {
			return plivoSIPPassword;
		}

		public void setPlivoSIPPassword(String plivoSIPPassword) {
			this.plivoSIPPassword = plivoSIPPassword;
		}

	}

	public static class Telnyx {
		private String domainUrl = "";
		private String token = "";
		private String baseAPIUrl = "";
		private String connectionId = "";

		public String getDomainUrl() {
			return domainUrl;
		}

		public void setDomainUrl(String domainUrl) {
			this.domainUrl = domainUrl;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public String getBaseAPIUrl() {
			return baseAPIUrl;
		}

		public void setBaseAPIUrl(String baseAPIUrl) {
			this.baseAPIUrl = baseAPIUrl;
		}

		public String getConnectionId() {
			return connectionId;
		}

		public void setConnectionId(String connectionId) {
			this.connectionId = connectionId;
		}

	}

	public static class Viva {
		private String domain = "";
		private String accountId = "";
		private String passcode = "";
		private String telnyxDomain = "";
		private String telnyxAccountId = "";
		private String telnyxPasscode = "";

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public String getAccountId() {
			return accountId;
		}

		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}

		public String getPasscode() {
			return passcode;
		}

		public void setPasscode(String passcode) {
			this.passcode = passcode;
		}

		public String getTelnyxDomain() {
			return telnyxDomain;
		}

		public void setTelnyxDomain(String telnyxDomain) {
			this.telnyxDomain = telnyxDomain;
		}

		public String getTelnyxAccountId() {
			return telnyxAccountId;
		}

		public void setTelnyxAccountId(String telnyxAccountId) {
			this.telnyxAccountId = telnyxAccountId;
		}

		public String getTelnyxPasscode() {
			return telnyxPasscode;
		}

		public void setTelnyxPasscode(String telnyxPasscode) {
			this.telnyxPasscode = telnyxPasscode;
		}
	}
	
	public static class Samespace {
		private String domain = "";
		private String username = "";
		private String password = "";

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
	}

	public static class Signalwire {
		private String projectId = "";
		private String applicationSid = "";
		private String authToken = "";
		private String signalwireBaseurl = "";
		private String signalwireTelnyxToken = "";
		
		public String getProjectId() {
			return projectId;
		}
		public void setProjectId(String projectId) {
			this.projectId = projectId;
		}
		public String getApplicationSid() {
			return applicationSid;
		}
		public void setApplicationSid(String applicationSid) {
			this.applicationSid = applicationSid;
		}
		public String getAuthToken() {
			return authToken;
		}
		public void setAuthToken(String authToken) {
			this.authToken = authToken;
		}
		public String getSignalwireBaseurl() {
			return signalwireBaseurl;
		}
		public void setSignalwireBaseurl(String signalwireBaseurl) {
			this.signalwireBaseurl = signalwireBaseurl;
		}
		public String getSignalwireTelnyxToken() {
			return signalwireTelnyxToken;
		}
		public void setSignalwireTelnyxToken(String signalwireTelnyxToken) {
			this.signalwireTelnyxToken = signalwireTelnyxToken;
		}
	}
}
