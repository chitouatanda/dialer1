(function() {
    'use strict';

    angular
        .module('xtaasApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
